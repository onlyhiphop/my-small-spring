package org.bcliao.beans.factory.annotation;

import java.lang.annotation.*;

/**
 * @author bcliao
 * @date 2022/2/7 10:17
 */
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Value {

    String value();
}
