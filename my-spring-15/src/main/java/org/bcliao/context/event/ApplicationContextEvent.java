package org.bcliao.context.event;

import org.bcliao.context.ApplicationEvent;

/**
 * @author bcliao
 * @date 2022/1/20 16:12
 */
public class ApplicationContextEvent extends ApplicationEvent {
    /**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public ApplicationContextEvent(Object source) {
        super(source);
    }
}
