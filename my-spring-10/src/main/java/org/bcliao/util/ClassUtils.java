package org.bcliao.util;

/**
 * @author bcliao
 * @date 2021/12/24 18:19
 */
public class ClassUtils {

    public static ClassLoader getDefaultClassLoader(){
        ClassLoader classLoader = null;
        try {
            classLoader = Thread.currentThread().getContextClassLoader();
        } catch (Exception e){

        }
        if(classLoader == null){
            classLoader = ClassLoader.class.getClassLoader();
        }
        return classLoader;
    }
}