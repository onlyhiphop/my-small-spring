package org.bcliao.core.convert.support;

import org.bcliao.core.convert.converter.ConverterRegistry;

/**
 * @author bcliao
 * @date 2022/3/8 13:39
 */
public class DefaultConversionService extends GenericConversionService{

    public DefaultConversionService(){
        addDefaultConverters(this);
    }

    public static void addDefaultConverters(ConverterRegistry converterRegistry){
        // 添加各类类型转换工厂
        converterRegistry.addConverterFactory(new StringToNumberConverterFactory());
    }
}
