package org.bcliao.test.bean;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @author bcliao
 * @date 2022/1/22 15:42
 */
public class UserService implements IUserService{
    @Override
    public String queryUserInfo() {
        try {
            TimeUnit.SECONDS.sleep(new Random(1).nextInt(5));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "小明，深圳";
    }

    @Override
    public String register(String userName) {
        try {
            TimeUnit.SECONDS.sleep(new Random(1).nextInt(5));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "注册用户：" + userName + "success! ";
    }
}
