package org.bcliao.factory.config;

/**
 * @author bcliao
 * @date 2021/12/21 10:41
 */
public interface SingletonBeanRegistry {
    Object getSingleton(String beanName);
    void registerSingletion(String beanName, Object singletonObject);
}
