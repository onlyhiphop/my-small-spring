package org.bcliao.example1;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author bcliao
 * @date 2021/12/20 10:04
 */
public class BeanFactory1 {

    /**
     * bean容器
     * key: 名称
     * value: 实例对象
     */
    private Map<String, Object> beanMap = new ConcurrentHashMap<>();

    public Object getBean(String name){
        return beanMap.get(name);
    }

    public void registerBeanDefinition(String name, Object obj){
        beanMap.put(name, obj);
    }
}
