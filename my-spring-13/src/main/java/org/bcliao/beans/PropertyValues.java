package org.bcliao.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * @author bcliao
 * @date 2021/12/24 10:23
 */
public class PropertyValues {

    private final List<PropertyValue> propertyValueList;

    public PropertyValues() {
        this.propertyValueList = new ArrayList<>();
    }

    public PropertyValue[] getPropertyValues() {
        return this.propertyValueList.toArray(new PropertyValue[0]);
    }

    public void addPropertyValue(PropertyValue pv){
        this.propertyValueList.add(pv);
    }

    /**
     * 通过属性名获取属性对象
     */
    public PropertyValue getPropertyValue(String propertyName){
        for (PropertyValue p :
                this.propertyValueList) {
            if (p.getBeanName().equals(propertyName)) {
                return p;
            }
        }
        return null;
    }
}
