package org.bcliao.test.bean;

/**
 * @author bcliao
 * @date 2022/1/18 21:11
 */
public interface IUserDao {

    String queryUserName(String uId);

}
