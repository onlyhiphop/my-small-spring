package org.bcliao.factory.support;

import org.bcliao.factory.config.SingletonBeanRegistry;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author bcliao
 * @date 2021/12/21 10:47
 */
public class DefaultSingletonBeanRegistry implements SingletonBeanRegistry {


    /**
     * 单例对象容器
     */
    private final Map<String, Object> singletonObjects = new ConcurrentHashMap<>(256);

    @Override
    public Object getSingleton(String beanName) {
        return singletonObjects.get(beanName);
    }

    @Override
    public void registerSingletion(String beanName, Object singletonObject) {
        singletonObjects.put(beanName, singletonObject);
    }


}
