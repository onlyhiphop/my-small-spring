package org.bcliao.test;

import org.bcliao.context.support.ClassPathXmlApplicationContext;
import org.bcliao.test.bean.IUserService;
import org.bcliao.test.bean.OrderService;
import org.bcliao.test.bean.ProductService;
import org.junit.Test;


/**
 * @author bcliao
 * @date 2022/2/18 13:58
 */
public class MainTest {

    /**
     * 测试 A -> B -> C -> A
     */
    @Test
    public void test_ioc(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring.xml");
        OrderService orderService = (OrderService) context.getBean("orderService");
        System.out.println(orderService.query("001"));
    }


    /**
     * 测试 AOP
     * 直接获取代理对象 IUserService
     */
    @Test
    public void test_aop(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring-scan.xml");
        IUserService userService = context.getBean("userService", IUserService.class);
        System.out.println(userService.query("001"));
    }

    /**
     * 改进后的二级缓存测试 AOP
     */
    @Test
    public void test_aop2(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring-scan.xml");
        ProductService productService = context.getBean("productService", ProductService.class);
        System.out.println(productService.query("001"));

        System.out.println(productService.userService);

        IUserService userService = context.getBean("userService", IUserService.class);
        System.out.println(userService);

        System.out.println(productService.query("002"));

    }

}
