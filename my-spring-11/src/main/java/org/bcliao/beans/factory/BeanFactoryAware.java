package org.bcliao.beans.factory;

import org.bcliao.beans.BeansException;

/**
 * 实现该接口，就能感知到所属的 BeanFactory
 * @author bcliao
 * @date 2022/1/14 10:30
 */
public interface BeanFactoryAware extends Aware{

    void setBeanFactory(BeanFactory beanFactory) throws BeansException;
}
