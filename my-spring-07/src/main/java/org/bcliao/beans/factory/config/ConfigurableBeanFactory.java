package org.bcliao.beans.factory.config;

import org.bcliao.beans.factory.HierarchicalBeanFactory;

/**
 * @author bcliao
 */
public interface ConfigurableBeanFactory extends HierarchicalBeanFactory, SingletonBeanRegistry {

    String SCOPE_SINGLETON = "singleton";
    String SCOPE_PROTOTYPE = "prototype";

    void addBeanPostProcessor(BeanPostProcessor beanPostProcessor);
}
