package org.bcliao.beans.factory.config;

import org.bcliao.beans.BeansException;

/**
 * @author bcliao
 * @date 2022/1/26 16:44
 */
public interface InstantiationAwareBeanPostProcessor extends BeanPostProcessor{

    /**
     * 在 Bean 对象实例化之前，执行此方法
     */
    Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException;
}
