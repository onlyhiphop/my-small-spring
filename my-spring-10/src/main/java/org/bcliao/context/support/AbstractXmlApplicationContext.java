package org.bcliao.context.support;

import org.bcliao.beans.factory.support.DefaultListableBeanFactory;
import org.bcliao.beans.factory.xml.XmlBeanDefinitionReader;

/**
 * @author bcliao
 * @date 2022/1/10 10:34
 */
public abstract class AbstractXmlApplicationContext extends AbstractRefreshableApplicationContext {

    @Override
    protected void loadBeanDefinitions(DefaultListableBeanFactory beanFactory) {
        XmlBeanDefinitionReader beanDefinitionReader = new XmlBeanDefinitionReader(beanFactory, this);
        String[] configLocations = getConfigLocations();
        if(null != configLocations){
            beanDefinitionReader.loadBeanDefinitions(configLocations);
        }
    }

    protected abstract String[] getConfigLocations();
}
