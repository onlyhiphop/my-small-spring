package org.bcliao.context.support;

import org.bcliao.beans.BeansException;
import org.bcliao.beans.factory.config.BeanPostProcessor;
import org.bcliao.context.ApplicationContextAware;
import org.bcliao.context.ConfigurableApplicationContext;

/**
 * @author bcliao
 * @date 2022/1/14 15:39
 */
public class ApplicationContextAwareProcessor implements BeanPostProcessor {

    private final ConfigurableApplicationContext applicationContext;

    public ApplicationContextAwareProcessor(ConfigurableApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

        if(bean instanceof ApplicationContextAware){
            ((ApplicationContextAware) bean).setApplicationContext(applicationContext);
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }
}
