#### IOC

---

##### 前言

一、在Spring中，提供两种方式给用户自定义Bean的性质

> https://docs.spring.io/spring-framework/docs/current/reference/html/core.html#beans-factory-lifecycle

1、生命周期回调

- 实现 `InitializingBean` 和 `DisposableBean` 接口
- JSR-250的 `@PostConstruct` 和 `@PreDestory` 注释 （这里先不实现）
- xml配置属性 `init-method` 和 `destory-method`



2、Aware 接口

- `ApplicationContextAware` 和 `BeanNameAware`
- 其他 Aware 接口



二、在非 web 应用程序中优雅关闭 Spring IOC 容器

​	为什么是非Web应用程序？因为Spring的Web程序已经有了相关的代码实现优雅的关闭了。



`ConfigurableApplicationContext` 接口已经实现了该方法，只需要直接调用

```java
ConfigurableApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
ctx.registerShutdownHook();
//添加一个JVM关闭钩子 里面已经实现了对各类的资源的释放
```

底层其实是使用了JDK提供的

```java
Runtime.getRuntime().addShutdownHook(Thread thread);

// 以下场景会被调用
//1、程序正常退出
//2、使用 System.exit()
//3、系统关闭
//4、使用 Ctrl + C
//5、使用 kill pid 干掉进程 （这里注意 kill -9 是不会被调用的）
//6、OutOfMemory宕机
```



##### 目标

​	1、实现生命周期的回调

​	2、实现优雅的关闭 Spring IOC容器



##### 设计

1、生命周期回调

- `InitializingBean` 、 `DesposableBean` 接口

  实体类只需要继承该接口实现其方法

- `init-method` 、`destroy-method` 配置

  属于类的配置信息，需要在 BeaDefinition 中加入 `initMethodName` 、`destoryMethodName` ，当解析配置文件时记录下来。

初始化处理（`InitializingBean` 、`init-method` ）：

​	逻辑写在上一章我们留下一个执行Bean初始化方法 `invokeInitMethods` 中。所以生命周期的初始化回调是在 BeanPostProcessor.Before 之后，BeanPostProcessor.After之前。

销毁处理（`DesposableBean`、`destroy-method`）：

- `ConfigurableApplication` 接口定义 `registerShutdownHook` 和 `close` 

- `ConfigurableBeanFactory` 接口定义 `destroySingletons` 方法

  但是它的实现却在 `AbstractBeanFactory` 的父类 `DefaultSingletonBeanRegistry`中；本来是谁去实现接口，谁就去实现它的抽象方法，但是这里将抽象方法的实现交给其父类去实现；这是分层服务的设计方式。

  虽然我们完全可以将这个操作放在 `AbstractBeanFactory` 中操作，因为它既继承了 `DefaultSingletonBeanRegistry` 拥有单例容器，也实现了 `ConfigurableBeanFactory` 拥有 `destroySingletons` 方法，但是这样的话，在功能分化上就不是特别合理，对于 `DefaultSingletonBeanRegistry` 类它的作用就是定位对单例容器的操作，所以创建销毁都应该在这个类里面。

  

![image-20220113134658837](img/image-20220113134658837.png)



- `DisposableBeanAdapter` 对继承了 `DisposableBean` 和  `destory-method` 的类统一进行包装适配



##### 测试

`org.bcliao.test.MainTest`



##### 总结

​	在类之间的关系越来越复杂后，我们要坚持对类的功能定位规范清晰，不要把不属于该类的功能混加进来，每个类的职责应该分明，这样在后续的扩展中才不会越来越乱。

