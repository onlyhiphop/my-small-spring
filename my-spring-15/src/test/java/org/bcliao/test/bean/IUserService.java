package org.bcliao.test.bean;

/**
 * @author bcliao
 * @date 2022/2/11 10:46
 */
public interface IUserService {

    String queryUserInfo();
}
