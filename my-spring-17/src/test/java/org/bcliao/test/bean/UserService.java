package org.bcliao.test.bean;


import org.bcliao.beans.factory.annotation.Autowired;
import org.bcliao.stereotype.Component;

/**
 * @author bcliao
 * @date 2022/2/18 13:58
 */
@Component
public class UserService implements IUserService {

    public UserService(){
        System.out.println("UserService---construct---");
    }

    @Autowired
    private OrderService orderService;

    public String query(String uId){
        return "User调用: " + uId;
    }
}
