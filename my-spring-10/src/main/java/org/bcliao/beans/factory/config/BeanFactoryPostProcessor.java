package org.bcliao.beans.factory.config;

import org.bcliao.beans.BeansException;
import org.bcliao.beans.factory.ConfigurableListableBeanFactory;

/**
 * @author bcliao
 * @date 2022/1/7 15:32
 */
public interface BeanFactoryPostProcessor {

    /**
     * 在所有的 BeanDefinition 加载完成后，实例化 Bean 对象之前， 提供修改 BeanDefinition 属性的机制
     * @param beanFactory
     * @throws BeansException
     */
    void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException;

}
