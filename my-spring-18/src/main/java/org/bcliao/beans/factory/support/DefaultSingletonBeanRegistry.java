package org.bcliao.beans.factory.support;

import org.bcliao.beans.BeansException;
import org.bcliao.beans.factory.DisposableBean;
import org.bcliao.beans.factory.ObjectFactory;
import org.bcliao.beans.factory.config.SingletonBeanRegistry;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author bcliao
 * @date 2021/12/21 10:47
 */
public class DefaultSingletonBeanRegistry implements SingletonBeanRegistry {


    /**
     * 单例对象容器
     */
    private final Map<String, Object> singletonObjects = new ConcurrentHashMap<>(256);

    /**
     * 二级缓存，缓存半成品对象
     */
    private final Map<String, Object> earlySingletonObjects = new HashMap<>(16);

    /**
     * 三级缓存，存下 创建代理对象操作
     */
    private final Map<String, ObjectFactory<?>> singletonFactories = new HashMap<>(16);

    /**
     * 实现 DisposableBean 接口的Bean
     */
    private final Map<String, DisposableBean> disposableBeans = new LinkedHashMap<>();

    @Override
    public Object getSingleton(String beanName) {
        Object bean = singletonObjects.get(beanName);
        if (bean == null) {
            //判断二级缓存中是否有对象
            bean = earlySingletonObjects.get(beanName);
            if(bean == null){
                ObjectFactory<?> singletonFactory = singletonFactories.get(beanName);
                if(null != singletonFactory){
                    bean = singletonFactory.getObject();
                    //放入二级缓存中
                    earlySingletonObjects.put(beanName, bean);
                    //删除三级缓存
                    singletonFactories.remove(beanName);
                }
            }
        }
        return bean;
    }

    @Override
    public void registerSingleton(String beanName, Object singletonObject) {
        singletonObjects.put(beanName, singletonObject);
        // 成品有了之后，删除二级、三级缓存
        earlySingletonObjects.remove(beanName);
        singletonFactories.remove(beanName);
    }

    protected void addSingletonFactory(String beanName, ObjectFactory<?> singletonFactory){
        if(!this.singletonObjects.containsKey(beanName)){
            this.singletonFactories.put(beanName, singletonFactory);
            // 移除没创建代理的半成品对象
            this.earlySingletonObjects.remove(beanName);
        }
    }

    public void registerDisposableBean(String beanName, DisposableBean bean){
        disposableBeans.put(beanName, bean);
    }

    public void destroySingletons(){
        Set<String> keySet = this.disposableBeans.keySet();
        String[] disposableBeanNames = keySet.toArray(new String[0]);

        for(int i = disposableBeanNames.length - 1; i >= 0; i--){
            String beanName = disposableBeanNames[i];
            DisposableBean disposableBean = this.disposableBeans.remove(beanName);
            try {
                disposableBean.destroy();
            } catch (Exception e) {
                throw new BeansException("Destroy method on bean with name '" + beanName + "' threw an exception", e);
            }
        }

        singletonObjects.clear();
    }
}
