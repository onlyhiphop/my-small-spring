package org.bcliao.context.support;

import cn.hutool.core.lang.Assert;
import org.bcliao.beans.BeansException;
import org.bcliao.beans.factory.ConfigurableListableBeanFactory;
import org.bcliao.beans.factory.config.BeanFactoryPostProcessor;
import org.bcliao.beans.factory.config.BeanPostProcessor;
import org.bcliao.context.ConfigurableApplication;
import org.bcliao.core.io.DefaultResourceLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author bcliao
 * @date 2022/1/4 10:46
 */
public abstract class AbstractApplicationContext extends DefaultResourceLoader implements ConfigurableApplication {

    /** JVM关闭钩子 */
    private Thread shutdownHook;
    /** 直接在容器中注册的 BeanFactoryPostProcessor. */
    private final List<BeanFactoryPostProcessor> beanFactoryPostProcessors = new ArrayList<>();

    @Override
    public void refresh() throws BeansException {
        //1. 创建 BeanFactory，并加载 BeanDefinition
        refreshBeanFactory();

        //2. 获取 BeanFactory
        ConfigurableListableBeanFactory beanFactory = getBeanFactory();

        //3. 在 Bean 实例化之前执行 BeanFactoryPostProcessor
        invokeBeanFactoryPostProcessors(beanFactory);

        //4.注册后置处理器 BeanPostProcessor  最后执行器在 Bean 对象实例化之前执行
        registerBeanPostProcessors(beanFactory);

        //5. 提前实例化单例 Bean 对象
        beanFactory.preInstantiateSingletons();
    }

    /**
     * 子类必须实现这个方法来执行实际的配置加载。
     */
    protected abstract void refreshBeanFactory() throws BeansException;

    protected abstract ConfigurableListableBeanFactory getBeanFactory();

    private void registerBeanPostProcessors(ConfigurableListableBeanFactory beanFactory) {
        Map<String, BeanPostProcessor> beanPostProcessorMap = beanFactory.getBeansOfType(BeanPostProcessor.class);
        for (BeanPostProcessor beanPostProcessor : beanPostProcessorMap.values()) {
            beanFactory.addBeanPostProcessor(beanPostProcessor);
        }
    }

    private void invokeBeanFactoryPostProcessors(ConfigurableListableBeanFactory beanFactory) {
        //1、先遍历执行已经在容器中手动注册的
        for (BeanFactoryPostProcessor beanFactoryPostProcessor : getBeanFactoryPostProcessors()) {
            beanFactoryPostProcessor.postProcessBeanFactory(beanFactory);
        }

        //2、再找在ioc容器创建的 BeanFactoryPostProcessor
        Map<String, BeanFactoryPostProcessor> beanFactoryPostProcessorMap = beanFactory.getBeansOfType(BeanFactoryPostProcessor.class);
        for (BeanFactoryPostProcessor beanFactoryPostProcessor : beanFactoryPostProcessorMap.values()) {
            beanFactoryPostProcessor.postProcessBeanFactory(beanFactory);
        }
    }

    @Override
    public Object getBean(String beanName) throws BeansException {
        return getBeanFactory().getBean(beanName);
    }

    @Override
    public Object getBean(String beanName, Object... args) throws BeansException {
        return getBeanFactory().getBean(beanName, args);
    }


    @Override
    public boolean containsBeanDefinition(String beanName) {
        return getBeanFactory().containsBeanDefinition(beanName);
    }

    @Override
    public <T> Map<String, T> getBeansOfType(Class<T> type) throws BeansException {
        return getBeanFactory().getBeansOfType(type);
    }

    @Override
    public String[] getBeanDefinitionNames() {
        return getBeanFactory().getBeanDefinitionNames();
    }

    public List<BeanFactoryPostProcessor> getBeanFactoryPostProcessors() {
        return beanFactoryPostProcessors;
    }

    @Override
    public void addBeanFactoryPostProcessor(BeanFactoryPostProcessor postProcessor) {
        Assert.notNull(postProcessor, "BeanFactoryPostProcessor must not be null");
        this.beanFactoryPostProcessors.add(postProcessor);
    }

    @Override
    public void registerShutdownHook() {
        if(this.shutdownHook == null){
            this.shutdownHook = new Thread(this::close);
            Runtime.getRuntime().addShutdownHook(shutdownHook);
        }
    }

    @Override
    public void close() {
        doClose();
        // 移除关闭钩子， 这里不加锁会报错
//        if(this.shutdownHook != null){
//            Runtime.getRuntime().removeShutdownHook(shutdownHook);
//        }
    }

    /**
     * 关闭资源
     */
    protected void doClose(){
        getBeanFactory().destroySingletons();
    }
}
