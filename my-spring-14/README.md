#### AOP

---

​	

##### 目标

1. 简化 Bean 对象的配置，实现使用注解自动注册 Bean 对象
2. 实现 xml 中的占位符配置



##### 设计

1. 实现使用注解注册 Bean 对象

创建注解 `@Component`  和 `@Scope`  ，加入解析 `component-scan` 标签逻辑， `ClassPathScanningCandidateComponentProvider` 扫描注册所有 `@Component` 注解的对象；它的子类 `ClassPathBeanDefinitionScanner` 除了获取 Bean 的类信息后，还需要获取 `@Scope`  配置的Bean 的作用域 和 beanName ，是单例模式的话将其注册到单例容器中。

2. 实现 xml 占位符配置

要在 Bean 实例化之前去替换 BeanDefinition 中属性的值，所以我们创建 `PropertyPlaceholderConfigurer` 继承 `BeanFactoryPostProcessor` 。



##### 结构图

![image-20220128134913730](img/image-20220128134913730.png)



##### 测试

org.bcliao.test.MainTest



##### 总结

​	可以发现我们在补充新功能都是在 IOC 和 AOP 核心的基础上来补全，BeanFactoryPostProcessor 、 BeanPostProcessor 、Aware 等等一系列扩展接口发挥着重要的作用，由此也可以体会到 Spring 强大的扩展性。

