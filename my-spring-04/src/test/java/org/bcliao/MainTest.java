package org.bcliao;

import org.bcliao.beans.PropertyValue;
import org.bcliao.beans.PropertyValues;
import org.bcliao.beans.factory.config.BeanDefinition;
import org.bcliao.beans.factory.config.BeanReference;
import org.bcliao.beans.factory.support.DefaultListableBeanFactory;
import org.bcliao.service.UserDao;
import org.bcliao.service.UserService;
import org.junit.Test;

/**
 * @author bcliao
 * @date 2021/12/24 11:32
 */
public class MainTest {

    @Test
    public void test_BeanFactory(){
        //初始化工厂
        DefaultListableBeanFactory factory = new DefaultListableBeanFactory();

        //先将dao注册
        factory.registerBeanDefinition("userDao", new BeanDefinition(UserDao.class));

        //设置UserService的属性
        PropertyValues propertyValues = new PropertyValues();
        propertyValues.addPropertyValue(new PropertyValue("uId", "001"));
        propertyValues.addPropertyValue(new PropertyValue("userDao", new BeanReference("userDao")));

        //注册UserService， BeanDefinition加入PropertyValues，将会自动注入
        factory.registerBeanDefinition("userService", new BeanDefinition(UserService.class, propertyValues));

        UserService userService = (UserService) factory.getBean("userService");
        userService.queryUserInfo();
    }
}
