package org.bcliao.beans.factory.config;

import org.bcliao.beans.BeansException;
import org.bcliao.beans.PropertyValues;

/**
 **{@link BeanPostProcessor}的子接口，该接口添加了一个before实例化回调，
 * 以及在实例化之后但在设置或删除显式属性之前的回调
 *
 * @author bcliao
 * @date 2022/1/26 16:44
 */
public interface InstantiationAwareBeanPostProcessor extends BeanPostProcessor{

    /**
     * 在 Bean 对象实例化之前，执行此方法
     */
    Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException;

    /**
     * 在 Bean 对象执行初始化方法之后，执行此方法
     */
    boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException;

    /**
     * 在 Bean 对象实例化完成后，设置属性操作之前执行此方法
     */
    PropertyValues postProcessPropertyValues(PropertyValues pvs, Object bean, String beanName) throws BeansException;

    /**
     * 在Spring中由 SmartInstantiationAwareBeanPostProcessor#getEarlyBeanReference 提供
     */
    default Object getEarlyBeanReference(Object bean, String beanName){
        return bean;
    }
}
