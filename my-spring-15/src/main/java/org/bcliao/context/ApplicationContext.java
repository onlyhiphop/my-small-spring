package org.bcliao.context;

import org.bcliao.beans.factory.HierarchicalBeanFactory;
import org.bcliao.beans.factory.ListableBeanFactory;
import org.bcliao.core.io.ResourceLoader;

/**
 * @author bcliao
 * @date 2022/1/4 10:29
 */
public interface ApplicationContext extends ListableBeanFactory, HierarchicalBeanFactory, ResourceLoader, ApplicationEventPublisher {
}
