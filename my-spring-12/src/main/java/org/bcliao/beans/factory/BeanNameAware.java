package org.bcliao.beans.factory;

/**
 * 实现此接口，就能感知到所属的 beanName
 * @author bcliao
 * @date 2022/1/14 10:31
 */
public interface BeanNameAware extends Aware{

    void setBeanName(String beanName);
}
