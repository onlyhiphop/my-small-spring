package org.bcliao.core.convert.converter;

/**
 * 类型转换工厂
 * @author bcliao
 * @date 2022/3/8 9:55
 */
public interface ConverterFactory<S, R> {

    <T extends R> Converter<S, T> getConverter(Class<T> targetType);
}
