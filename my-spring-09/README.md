#### IOC

---

​	目前只是针对 Bean 定义和注册做一些扩展处理，我们无法在运行中获取 Spring 提供的 BeanFactory 、ApplicationContext、BeanClassLoader，所以本章提供一个能感知容器操作的接口。只要实现了该接口，就能获取到各类。



##### 目标

Award接口及其子接口



##### 设计

- 建立 `Aware` 接口以及其他需要感知接口
- `BeanFactoryAware` 、`BeanNameAware` 、`BeanClassLoaderAware` 都可以在 `createBean` 的时候获取
- `ApplicationContextAware`  只能在 `refresh` 中去获取，既要拿到 `ApplicationContext` 也要拿到 `Bean` ，所以我们使用一个 `ApplicationContextAwareProcessor` 后置处理器 



测试

org.bcliao.MainTest



##### 结构图

![image-20220114100819024](img/image-20220114100819024.png)



##### 总结

​	Spring 留给我们非常多可扩展接口（牛x之处就在于它为使用者设计了非常多可能会使用到的扩展—— spring yyds!），这也让它和其他框架的组合变得简单。

