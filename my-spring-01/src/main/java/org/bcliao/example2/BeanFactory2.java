package org.bcliao.example2;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author bcliao
 * @date 2021/12/20 10:48
 */
public class BeanFactory2 {
    /**
     * bean容器
     * key: 名称
     * value: 实例对象
     */
    private Map<String, BeanDefinition> definitionMap = new ConcurrentHashMap<>();

    public Object getBean(String name){
        return definitionMap.get(name).getBean();
    }

    public void registerBeanDefinition(String name, BeanDefinition obj){
        definitionMap.put(name, obj);
    }
}
