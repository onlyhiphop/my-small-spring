#### AOP

---

​	在之前把 AOP 动态代理融入到 Bean 的生命周期时，创建代理对象是在整个创建 Bean 对象之前，而且创建被代理对象时也是手动创建的，并不是 IOC 创建的，所以说这个代理对象的创建并不是在 Bean 生命周期中。


##### 目标

​	将代理对象的创建融入到 Bean 的生命周期中，把创建代理对象的逻辑迁移到 Bean 对象执行初始化方法之后，再执行代理对象的创建，实现代理对象的属性注入。


##### 结构图
![image1](img/Snipaste_2022-02-18_09-15-43.png)


##### 设计
- 我们可以利用 `BeanPostProcessor#postProcessAfterInitialization`方法 可在 Bean 实例化且属性注入之后生成代理对象，这样的 被代理对象 就融入到了 Bean 的生命周期。
- 在`DefaultAdvisorAutoProxyCreator#postProcessAfterInitialization` 实现代理对象的生成，这时传入的 bean 已经由 IOC 创建和属性注入完毕。
- 还有注意的地方是 `TargetSource#getTargetClass` 是用来获取 target 对象的接口信息，这个被代理对象由于融进了 IOC 中，所以它可能是 Cglib 创建的，如果是 cglib 策略实例化的对象，那它真正的类则需要 `clazz.getSuperclass()` 获取（因为 cglib 实例化时是继承原来的类）


##### 测试

org.bcliao.test.MainTest


##### 总结

​	我们应熟记这些扩展接口，清楚这些接口在 Bean 生命周期中处于什么位置，这对于我们在后续如果需要扩展 spring 将非常重要。