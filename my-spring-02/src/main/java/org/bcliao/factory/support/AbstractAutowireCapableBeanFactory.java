package org.bcliao.factory.support;

import org.bcliao.factory.BeansException;
import org.bcliao.factory.config.BeanDefinition;

/**
 * @author bcliao
 * @date 2021/12/22 9:30
 */
public abstract class AbstractAutowireCapableBeanFactory extends AbstractBeanFactory {

    @Override
    protected Object createBean(String beanName, BeanDefinition beanDefinition) throws BeansException {

        Object bean;
        try {
            bean = beanDefinition.getBeanClass().getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            throw new BeansException("Instantiation of bean failed", e);
        }
        // 创建后，放入单例容器中
        registerSingletion(beanName, bean);
        return bean;
    }
}
