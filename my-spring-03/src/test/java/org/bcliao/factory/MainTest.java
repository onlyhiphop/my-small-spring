package org.bcliao.factory;

import org.bcliao.factory.bean.UserService;
import org.bcliao.factory.config.BeanDefinition;
import org.bcliao.factory.support.DefaultListableBeanFactory;
import org.bcliao.factory.support.SimpleInstantiationStrategy;
import org.junit.Test;

/**
 * @author bcliao
 * @date 2021/12/23 11:29
 */
public class MainTest {

    @Test
    public void test_DefaultListableBeanFactory(){
        // 初始化工厂
        DefaultListableBeanFactory factory = new DefaultListableBeanFactory();
        // 注入bean
        BeanDefinition beanDefinition = new BeanDefinition(UserService.class);
        factory.registerBeanDefinition("userService", beanDefinition);

        //可以修改实例化策略
        factory.setInstantiationStrategy(new SimpleInstantiationStrategy());

        //获取Bean
        UserService bean = (UserService) factory.getBean("userService", "参数1");
        bean.queryUserInfo();

        //再次获取bean
        UserService bean2 = (UserService) factory.getBean("userService", "参数1");

        System.out.println(bean == bean2);
    }

}
