package org.bcliao.context.support;

import org.bcliao.beans.BeansException;
import org.bcliao.beans.factory.support.DefaultListableBeanFactory;

/**
 * @author bcliao
 * @date 2022/1/10 10:25
 */
public abstract class AbstractRefreshableApplicationContext extends AbstractApplicationContext {

    /** Bean factory for this context. */
    private DefaultListableBeanFactory beanFactory;

    @Override
    public void refreshBeanFactory() throws BeansException {
        DefaultListableBeanFactory beanFactory = createBeanFactory();
        loadBeanDefinitions(beanFactory);
        this.beanFactory = beanFactory;
    }

    protected abstract void loadBeanDefinitions(DefaultListableBeanFactory beanFactory);

    protected DefaultListableBeanFactory createBeanFactory(){
        return new DefaultListableBeanFactory();
    }

    @Override
    public DefaultListableBeanFactory getBeanFactory() {
        return beanFactory;
    }
}
