package org.bcliao.beans.factory.support;

import cn.hutool.core.util.StrUtil;
import org.bcliao.beans.BeansException;
import org.bcliao.beans.factory.DisposableBean;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author bcliao
 * @date 2022/1/13 15:46
 */
public class DisposableBeanAdapter implements DisposableBean {

    private final Object bean;
    private final String beanName;
    private String destroyMethodName;

    public DisposableBeanAdapter(Object bean, String beanName, String destroyMethodName) {
        this.bean = bean;
        this.beanName = beanName;
        this.destroyMethodName = destroyMethodName;
    }

    @Override
    public void destroy() throws Exception {
        //1、如果是实现接口 DisposableBean
        if(bean instanceof DisposableBean){
            ((DisposableBean)bean).destroy();
        }
        //2、注解配置 destroy-method
        if(StrUtil.isNotEmpty(destroyMethodName) &&
                !(bean instanceof DisposableBean && "destroy".equals(this.destroyMethodName)) //防止重复执行销毁
        ){
            try {
                Method destroyMethod = bean.getClass().getMethod(destroyMethodName);
                destroyMethod.invoke(bean);
            } catch (NoSuchMethodException e) {
                throw new BeansException("Couldn't find a destroy method named '" + destroyMethodName + "' on bean with name '" + beanName);
            }
        }
    }

}
