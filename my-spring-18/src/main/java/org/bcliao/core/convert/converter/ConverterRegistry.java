package org.bcliao.core.convert.converter;

/**
 * 类型转换注册接口
 * @author bcliao
 * @date 2022/3/8 10:00
 */
public interface ConverterRegistry {

    /**
     * 注册一个普通的转换器
     */
    void addConverter(Converter<?, ?> converter);

    /**
     * 注册一个通用的转换器
     */
    void addConverter(GenericConverter converter);

    /**
     * 注册一个远程转换器工厂
     */
    void addConverterFactory(ConverterFactory<?, ?> converterFactory);
}
