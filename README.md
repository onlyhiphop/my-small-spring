## :bookmark_tabs: 学习说明

​	本仓库以 Spring 源码学习为目的，从简单设计再到使用设计模式优化，一步一步带你手写简化版的 Spring 框架，深入理解 Spring 框架的核心原理。



## :exclamation: 阅前说明​

​	本仓库思路和说明都是个人见解（键盘侠手下留情），如有错误，请谅解并指正。



## :key: 阅读方法

​	1、在每一次版本模块都有一个 README.md 记录了当时版本的我的理解思路和步骤，读者可根据讲解思路自己敲一遍，加深理解。

​	2、随着类越来越多，整个结构也变得复杂，建议每个章节都用 IDEA 画出UML类图，对着UML类图去写代码，能帮助你更好理清类之间的关系。

​	

## :rose: 参考资料​	

站在巨人的肩膀上学习

> https://github.com/fuzhengwei/small-spring
>
> https://gitee.com/zhong96/spring-framework-analysis
>
> https://github.com/spring-projects/spring-framework

