#### IOC

---

​	在我们使用 Mybatis 时，我们不需要去实现 Dao 的接口，仅仅通过 Dao 接口就可以去操作数据库，它是怎么做到的呢？而我们在spring中使用时能直接注入dao接口，说明在spring容器中是存在一个具体的实现类的。

​	其实在 mybaits 结合 spring包中，存在一个 `MapperFactoryBean ` 类，它在 getObject 方法中提供 SqlSession 执行 CRUD操作。

​	FactoryBean 接口实际解决实例化 bean 过程比较复杂的场景，按照传统方式（xml配置、@Bean注解）创建可能不够灵活，我们就可以采用 实现 FactoryBean ，通过编码方式实例化对象。

​	

##### 目标

1、实现原型（prototype）模式

2、FactoryBean



设计

1、实现原型模式

- `BeanDefinition` 中新增两个 scope 属性
- 单例模式和原型模式的区别在于是否存放到内存中，单例模式已经有了一个Map缓存下来 ，所以我们只需要在加入单例容器的时候判断一个该 `BeanDefinition` 是否是单例模式就行。
- 在非 Singleton 类型的bean是没有缓存的，所以不需要执行销毁方法，所以我们在 `registerDisposableBeanIfNecessary` 方法中判断一下，只有Singleton对象才会注册到存放  DisposableBean的map容器中。



2、实现 FactoryBean

- 定义 `FactoryBean` 接口
- 创建一个 `FactoryBeanRegistrySupport` 类继承 `DefaultSingletonBeanRegistry` ，这样设计是希望做到不同领域模块下只负责各自的功能。它主要处理 FactoryBean 的`getObject`中对象 的注册，如果是单例类型就缓存在Map中，如果不是就执行 `getObject` 返回对象。

- 那在Spring容器中，如何获取 FactoryBean对象？ 在 beanName 前加上 "&"；如： name="&aa" 获取的是 对应FactoryBean对象、name="aa" 获取的是 getObject 返回的对象。



3、实现FactoryBean中的后置处理器

​	BeanPostProcessor 的前置处理方法 `postProcessBeforeInitialization` 是没有作用的，因为 BeanPostProcessor 的执行在 IOC 创建 bean 后（`createBean` 方法中）就执行了，而FactoryBean对象的创建是在实现的 `getObject` 方法中，是在 `createBean` 后才执行的 （就是 `createBean` 是创建FactoryBean对象，而真实对象是要有 FactoryBean 中的 getObject 创建）。

​	实现后置处理方法 `postProcessAfterInitialization`

- 在 `FactoryBeanRegistrySupport` 中调用 `postProcessObjectFromFactoryBean` ，该方法留给子类 `AbstractAutowireCapableBeanFactory` 去实现，最后是通过调用 `applyBeanPostProcessorsAfterInitialization` 去实现



##### 结构图

![image-20220117221403595](img/image-20220117221403595.png)



##### 总结

​	又一个功能的补充，这里能感受到前期接口设计和功能划分带来的好处。

​	在扩展功能的时候，不能和旧功能混在一起，应该新创建一个类去继承旧功能扩展新方法，这样才遵循开闭原则。如果父类旧功能已经在多处应用，继承的子类也不应该去重写覆盖父类的方法，应该新起一个方法，这就是里氏替换原则 。

