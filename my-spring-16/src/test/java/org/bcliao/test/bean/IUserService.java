package org.bcliao.test.bean;

/**
 * @author bcliao
 * @date 2022/2/14 11:26
 */
public interface IUserService {

    String queryUserName(String uId);
}
