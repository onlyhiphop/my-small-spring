package org.bcliao.beans.factory;

/**
 * @author bcliao
 * @date 2022/1/12 11:11
 */
public interface DisposableBean {

    void destroy() throws Exception;
}
