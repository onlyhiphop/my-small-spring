package org.bcliao.context;

import org.bcliao.beans.factory.ListableBeanFactory;

/**
 * @author bcliao
 * @date 2022/1/4 10:29
 */
public interface ApplicationContext extends ListableBeanFactory {
}
