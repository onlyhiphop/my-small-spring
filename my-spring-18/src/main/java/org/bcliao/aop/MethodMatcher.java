package org.bcliao.aop;

import java.lang.reflect.Method;

/**
 *
 * @author bcliao
 * @date 2022/1/22 17:58
 */
public interface MethodMatcher {

    /**
     * 对类中的方法判断是否符合切入点表达式
     */
    boolean matches(Method method, Class<?> targetClass);

}
