package org.bcliao.beans.factory;

/**
 * @author bcliao
 * @date 2022/1/12 11:10
 *
 * 为 Bean初始化后(实例化、属性注入)提供执行方法
 */
public interface InitializingBean {

    /**
     * 在全部的属性注入后执行
     * @throws Exception
     */
    void afterPropertiesSet() throws Exception;
}
