package org.bcliao.beans.factory;

/**
 * @author bcliao
 */
public interface HierarchicalBeanFactory extends BeanFactory{
    BeanFactory getParentBeanFactory();
}
