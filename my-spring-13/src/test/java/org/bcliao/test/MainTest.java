package org.bcliao.test;

import org.bcliao.context.support.ClassPathXmlApplicationContext;
import org.bcliao.test.bean.IUserService;
import org.junit.Test;

/**
 * @author bcliao
 * @date 2022/1/26 17:55
 */
public class MainTest {

    @Test
    public void test_aop(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring.xml");
        IUserService userService = (IUserService) context.getBean("userService");
        userService.queryUserInfo();
    }

}
