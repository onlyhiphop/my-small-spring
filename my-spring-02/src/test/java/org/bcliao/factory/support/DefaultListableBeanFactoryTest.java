package org.bcliao.factory.support;

import org.bcliao.bean.UserService;
import org.bcliao.factory.config.BeanDefinition;
import org.junit.Test;

/**
 * @author bcliao
 * @date 2021/12/22 10:20
 */
public class DefaultListableBeanFactoryTest {

    @Test
    public void test_DefaultListableBeanFactory(){
        DefaultListableBeanFactory factory = new DefaultListableBeanFactory();
        // 单例容器不存在，先注册
        BeanDefinition beanDefinition = new BeanDefinition(UserService.class);
        factory.registerBeanDefinition("userService", beanDefinition);

        // 获取 bean
        UserService us1 = (UserService)factory.getBean("userService");

        // 从单例容器中获取 bean
        UserService us2 = (UserService)factory.getSingleton("userService");

        System.out.println(us1 == us2); //true


    }

}
