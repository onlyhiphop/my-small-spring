package org.bcliao.aop;

/**
 * 被代理对象
 * @author bcliao
 * @date 2022/1/22 22:22
 */
public class TargetSource {

    private final Object target;

    public TargetSource(Object target) {
        this.target = target;
    }

    public Class<?>[] getTargetClass(){
        return this.target.getClass().getInterfaces();
    }

    public Object getTarget(){
        return this.target;
    }
}
