#### IOC

---

​	在上一章中测试中，我们对于Bean对象的定义、注册、属性的注入，都是手动的创建操作，现在我们优化成直接在配置文件配置。



##### 设计

​	在设计该模块功能时，我们可以单独拎出来思考，多想想它之后可能会有的扩展。经过前几章的铺垫，肯定会想到先抽象成一个接口，对于抽象成接口，肯定是越细越好，一个接口最好不要代表太多的功能，不要建立臃肿的接口（接口隔离原则）。

先思考资源加载部分：

1. 要去将配置文件中的信息读出来要有加载器，抽象成 `ResourceLoader` 接口
2. 关于资源的来源，可以来自本地文件File，classpath，http文件（远程云文件），抽象成 `Resource` 
3. `ResourceLoader` 引用 `Resource` ，这样我们既可以对loader进行扩展，也可以对资源 Resource 进行扩展。

将上面资源加载部分关联到之前代码：

​	思路：此处为新增资源加载扩展功能，我们其实也就是想扩展 `registerBeanDefinition` 方法，就是往注册表中注册 BeanDefinition 的过程。在之前我们已经将注册 BeanDefinition 抽象成了接口 `BeanDefinitionRegistry` ，所以我们只需要对 `BeanDefinitionRegistry` 接口编程就可以（从这里可以感受到为啥之前要将某一可能扩展的功能接口化）。

1. 要对之前注册 BeanDefition 功能增强（也就是`registerBeanDefinition` 方法 ），先建立一个中间接口（老套路抽象成接口）`BeanDefinitionReader` 将`ResourceLoader` 和 `BeanDefinitionRegistry` 关联起来 
2. 然后又设置一个模板类（和之前章节一样的模板模式）`AbstractBeanDefinitionReader`  
3. 这里创建 xml 的方式加载 `XmlBeanDefinitionReader`



##### 结构图

![image-20211227100949254](img/image-20211227100949254.png)

![image-20211227100851389](img/image-20211227100851389.png)



##### 测试

​	自行看test包下的代码



##### 总结

​	不用去死记到底该怎么写，学习其中的设计思路和思想，平时写代码的时候多按照这种思路去写，写的多了，自然就有感觉了，没有谁开始就能设计出完美的结构，都是靠不停的积累。

