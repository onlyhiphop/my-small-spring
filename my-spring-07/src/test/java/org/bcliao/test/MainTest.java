package org.bcliao.test;

import org.bcliao.context.support.ClassPathXmlApplicationContext;
import org.bcliao.test.bean.UserService;
import org.junit.Test;

/**
 * @author bcliao
 * @date 2022/1/10 11:12
 */
public class MainTest {

    @Test
    public void test_ClassPathXmlApplicationContext(){
        // 初始化容器 （里面就已经执行了 初始化BeanFactory 加载BeanDefinition）
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring.xml");

        //获取Bean
        UserService userService = (UserService) applicationContext.getBean("userService", UserService.class);

        String s = userService.queryUserInfo();

        System.out.println("结果：");
        System.out.println(s);
    }
}
