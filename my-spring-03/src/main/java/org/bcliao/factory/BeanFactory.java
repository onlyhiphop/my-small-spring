package org.bcliao.factory;

/**
 * @author bcliao
 * @date 2021/12/21 9:59
 */
public interface BeanFactory {
    Object getBean(String beanName) throws BeansException;
    Object getBean(String beanName, Object... args) throws BeansException;
}
