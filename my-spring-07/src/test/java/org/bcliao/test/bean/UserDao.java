package org.bcliao.test.bean;

import java.util.HashMap;
import java.util.Map;

/**
 * @author bcliao
 * @date 2021/12/27 16:57
 */
public class UserDao {

    private static Map<String, String> hashMap = new HashMap<>();

    static {
        hashMap.put("001", "AAA");
        hashMap.put("002", "BBB");
        hashMap.put("003", "CCC");
    }

    public String queryUserName(String uId) {
        return hashMap.get(uId);
    }
}
