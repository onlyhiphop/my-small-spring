package org.bcliao.beans.factory.support;

import org.bcliao.beans.BeansException;
import org.bcliao.beans.factory.config.BeanDefinition;

import java.lang.reflect.Constructor;

/**
 * @author bcliao
 * @date 2021/12/23 11:08
 */
public class SimpleInstantiationStrategy implements InstantiationStrategy{

    @Override
    public Object instantiate(BeanDefinition beanDefinition, String beanName, Constructor constructor, Object[] args) throws BeansException {
        Class beanClass = beanDefinition.getBeanClass();
        try {
            if(constructor != null){
                return beanClass.getDeclaredConstructor(constructor.getParameterTypes()).newInstance(args);
            }else{
                return beanClass.getDeclaredConstructor().newInstance();
            }
        }catch (Exception ex){
            throw new BeansException("Failed to instantiate [" + beanClass.getName() + "]", ex);
        }
    }
}
