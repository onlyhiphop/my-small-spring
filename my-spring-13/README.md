#### AOP

---

​	上一章节我们已经实现了 AOP 的核心功能，现在我们要把这部分功能融入到 Spring IOC 中，最终实现我们不需要手动去 new  一个 AdvisedSupport 去组装代理信息，用户只需要配置需要切入点，和编写需要增强的部分代码。



##### 设计

- 将拦截器功能拆分细化成一条拦截链路： 前置通知，后置通知 ，具体如下UML类图

![image-20220127111123925](img/image-20220127111123925.png)

（这里我们只实现左边部分：前置通知）

通过这样拆分可以简化用户使用切面功能，不用向上一章一样去创建一个拦截器

- 见切面表达式（Pointcut）和 切面增强（Advice）包装成 `AspectJExpressionPointcutAdvisor` ，用于存储一整个增强过程，放入IOC容器中，便于传输信息，后续通过 IOC 获取所有 `AspectJExpressionPointcutAdvisor` 就可以获取到整个代理过程信息。

![image-20220127161207251](img/image-20220127161207251.png)



- 创建 `ProxyFactory` 代理工厂，封装获取代理对象，根据条件判断用 JDK 还是 cglib 创建代理对象。
- 新增接口 `InstantiationAwareBeanPostProcessor` 继承 `BeanPostProcessor` 用来在处理 需要代理的 Bean 的实例化操作。 `DefaultAdvisorAutoProxyCreator` 是它的实现类，由它来实现 我们上一章 组装代理信息（AdvisedSupport） 传入 ProxyFactory 生成代理类返回，是将整个 AOP 代理融入 Bean 生命周期的核心类。
- 因为 `InstantiationAwareBeanPostProcessor` 继承 `BeanPostProcessor` 所以它会被放入 `BeanPostProcessor` 的存储List缓存中，执行 `InstantiationAwareBeanPostProcessor` 接口方法需要在 实例化对象之前通过 `DefaultAdvisorAutoProxyCreator` 判断是否匹配（匹配器）配置的条件从而生成代理对象。



##### 结构图

![image-20220126150004156](img/image-20220126150004156.png)



##### 测试

org.bcliao.test.MainTest



##### 总结

​	本章的重点就是在于怎么讲 AOP 和 Bean 生命周期结合起来，主要是用到了 `BeanPostProcessor` ，它可以在创建实例化对象的时候，实现对象的扩展点，由此可以见 spring 的扩展性设计是非常强的。这里我们实现的与 spring 源码核心逻辑类似，但是简化了很多，建议后续弄懂了整个AOP实现思路可以再去看看源码。





