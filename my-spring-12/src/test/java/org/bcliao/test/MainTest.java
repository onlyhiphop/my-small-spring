package org.bcliao.test;

import org.aopalliance.intercept.MethodInterceptor;
import org.bcliao.aop.AdvisedSupport;
import org.bcliao.aop.MethodMatcher;
import org.bcliao.aop.TargetSource;
import org.bcliao.aop.aspectj.AspectJExpressionPointcut;
import org.bcliao.aop.framework.CglibAopProxy;
import org.bcliao.aop.framework.JdkDynamicAopProxy;
import org.bcliao.aop.framework.ReflectiveMethodInvocation;
import org.bcliao.test.bean.IUserService;
import org.bcliao.test.bean.UserService;
import org.bcliao.test.bean.UserServiceInterceptor;
import org.junit.Test;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author bcliao
 * @date 2022/1/22 14:18
 */
public class MainTest {

    /**
     * 1阶段
     */
    @Test
    public void test_proxy_method(){
        // 目标对象 （可以替换成任何的目标对象）
        Object targetObj = new UserService();
        //AOP代理
        IUserService proxy = (IUserService) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
                targetObj.getClass().getInterfaces(),
                new InvocationHandler() {
                    // 方法匹配器
                    MethodMatcher methodMatcher = new AspectJExpressionPointcut("execution(* org.bcliao.test.bean.IUserService.*(..))");
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        if(methodMatcher.matches(method, targetObj.getClass())){
                            //方法拦截器
                            MethodInterceptor methodInterceptor = invocation -> {
                                long start = System.currentTimeMillis();
                                try {
                                    return invocation.proceed();
                                } finally {
                                    System.out.println("监控 - Begin By AOP");
                                    System.out.println("方法名称：" + invocation.getMethod().getName());
                                    System.out.println("方法耗时：" + (System.currentTimeMillis() - start) + "ms");
                                    System.out.println("监控 - End\r\n");
                                }
                            };
                            //反射调用
                            return methodInterceptor.invoke(new ReflectiveMethodInvocation(targetObj, method, args));
                        }
                        return method.invoke(targetObj, args);
                    }
                });
        String result = proxy.queryUserInfo();
        System.out.println("测试结果：" + result);
    }

    /**
     * 2阶段
     */
    @Test
    public void test_dynamic(){
        //目标对象
        IUserService userService = new UserService();
        //组装代理信息
        AdvisedSupport advisedSupport = new AdvisedSupport();
        advisedSupport.setTargetSource(new TargetSource(userService));
        advisedSupport.setMethodInterceptor(new UserServiceInterceptor());
        advisedSupport.setMethodMatcher(new AspectJExpressionPointcut("execution(* org.bcliao.test.bean.IUserService.*(..))"));

        //创建代理对象
        IUserService proxy_jdk = (IUserService) new JdkDynamicAopProxy(advisedSupport).getProxy();
        System.out.println("测试结果：" + proxy_jdk.queryUserInfo());

        IUserService proxy_cglib = (IUserService) new CglibAopProxy(advisedSupport).getProxy();
        System.out.println("测试结果：" + proxy_cglib.register("小红"));
    }

    @Test
    public void test_aop() throws NoSuchMethodException {
        AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut("execution(* org.bcliao.test.bean.IUserService.*(..))");

        Class<UserService> userServiceClass = UserService.class;
        System.out.println(pointcut.matches(userServiceClass));

        Method queryUserInfo = userServiceClass.getMethod("queryUserInfo");
        System.out.println(pointcut.matches(queryUserInfo, userServiceClass));
    }

}
