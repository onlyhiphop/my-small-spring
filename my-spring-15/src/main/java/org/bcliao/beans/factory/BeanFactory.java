package org.bcliao.beans.factory;

import org.bcliao.beans.BeansException;

/**
 * @author bcliao
 * @date 2021/12/21 9:59
 */
public interface BeanFactory {

    String FACTORY_BEAN_PREFIX = "&";

    Object getBean(String beanName) throws BeansException;
    Object getBean(String beanName, Object... args) throws BeansException;

    <T> T getBean(String name, Class<T> requiredType) throws BeansException;

    <T> T getBean(Class<T> requiredType) throws BeansException;
}
