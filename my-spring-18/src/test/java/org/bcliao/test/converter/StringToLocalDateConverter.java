package org.bcliao.test.converter;

import org.bcliao.core.convert.converter.Converter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author bcliao
 * @date 2022/3/11 14:11
 */
public class StringToLocalDateConverter implements Converter<String, LocalDateTime> {

    private final DateTimeFormatter DATE_TIME_FORMATTER;

    public StringToLocalDateConverter(String pattern) {
        DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(pattern);
    }


    @Override
    public LocalDateTime convert(String source) {
        return LocalDateTime.parse(source, DATE_TIME_FORMATTER);
    }
}
