package org.bcliao.test;


import org.bcliao.context.support.ClassPathXmlApplicationContext;
import org.bcliao.test.bean.IUserService;
import org.junit.Test;

/**
 * @author bcliao
 * @date 2022/2/9 17:04
 */
public class MainTest {

    @Test
    public void test_autowired(){
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring.xml");
        IUserService userService = applicationContext.getBean("userService", IUserService.class);
        System.out.println(userService.queryUserInfo());
    }

}
