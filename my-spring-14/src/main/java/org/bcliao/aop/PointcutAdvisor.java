package org.bcliao.aop;

/**
 * @author bcliao
 * @date 2022/1/26 15:59
 */
public interface PointcutAdvisor extends Advisor {

    /**
     * Get the Pointcut that drives this advisor.
     */
    Pointcut getPointcut();
}
