package org.bcliao.test.bean;

import org.bcliao.beans.factory.annotation.Autowired;
import org.bcliao.stereotype.Component;

/**
 * @author bcliao
 * @date 2022/2/18 17:27
 */
@Component
public class ProductService {

    public ProductService(){
        System.out.println("ProductService---construct---");
    }

    @Autowired
    public IUserService userService;

    public void initMethod(){
        System.out.println("ProductService initMethod---------");
    }

    public String query(String uId) {
        return "Product调用: " + userService.query(uId);
    }
}
