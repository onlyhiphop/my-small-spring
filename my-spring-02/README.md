#### IOC

使用设计模式重构

---

1、BeanDefinition缺陷

​	详情：`org.bcliao.factory.config.BeanDefinition`

​	之前设计的 BeanDefinition 里面直接关联的是一个实例对象，对于spring容器注册信息，我们应该注册一个类信息，而不是具体某个实例对象。



2、将 BeanFactory 接口化 

​	详情：`org.baliao.factory.BeanFactory`

​	接口化以便于后续对工厂的扩展；比如后续我们的 Bean 的创建可能会有多种不同的方式（BeanDefinition也可以这样接口化设计，但是这里就不做了）。

​	

3、单例设计

​	详情：`org.baliao.factory.config.SingletonBeanRegistry`

​	我们都知道spring容器默认就是单例的，所以我们将单例这部分的功能抽象成一个接口，需要用到单例的就继承这个接口。

​	再建一个单例默认实现类，里面放置Map单例对象容器 `DefaultSingletonBeanRegistry` ，之后所有的工厂拿单例对象都是从这个容器中拿（spring源码中也是由它的三级缓存解决经典的循环依赖问题）



4、使用模板模式

​	详情：`AbstractBeanFactory` 给 BeanFactory 接口设置一个默认的模板类

模板逻辑：先从单例容器中获取对象；如果不存在，则由后续继承模板类的具体工厂类去实现获取对象的方法，创建并返回对象。



5、BeanDefinition 设计

​	之前我们已经定义了单例容器，对象创建要通过 BeanDefinition ,所以我们也应该将 BeanDefinition 的各种操作也管理起来。同样的，抽出一个注册表接口 `BeanDefinitionRegistry` 。



6、分析 `DefaultListableBeanFactory`

​	最后我们得到了 `DefaultListableBeanFactory` 类，大家结合我上面的解释，可以先自己好好分析感受一波如此设计的魅力。

​	继承了 `AbstractBeanFactory` 模板类，模板类里面已经集成了先去单例容器取bean的操作，实现了 `BeanDefinitionRegistry` 注册表接口 ，表示，该类可以创建等操作 BeanDefinition 注册表。



7、测试

查看 `DefaultListableBeanFactoryTest` 测试类



#### 总结

​	通过本章，相信已经初步感受到了设计模式带来的晕头转向，接口、抽象类、继承、实现，一个程序的设计离不开这些，当我们接到一个简单的需求时，我们常常只想到眼前crud，没有长远的视野，所以每次写出来的代码都雷同，最后退化成cv开发。其实一些看似简单的需求，我们也可以放开视野，考虑长远发展。

