package org.bcliao.beans.factory;

/**
 * @author bcliao
 * @date 2022/1/16 17:12
 */
public interface FactoryBean<T> {

    /**
     * 返回由FactoryBean创建的Bean实例，如果 isSingleton() 返回 true
     * 则该实例会放到 spring 单例容器中
     */
    T getObject() throws Exception;

    /**
     * 返回FactoryBean创建的Bean实例类型
     */
    Class<?> getObjectType();

    /**
     * 返回由FactoryBean 创建的Bean实例的作用域是singleton还是prototype
     */
    default boolean isSingleton(){return true;}
}
