package org.bcliao.test.bean;

/**
 * @author bcliao
 * @date 2022/2/22 9:48
 */
public interface IUserService {
    String query(String uId);
}
