package org.bcliao.test.converter;

import org.bcliao.core.convert.converter.Converter;

/**
 * @author bcliao
 * @date 2022/3/11 14:43
 */
public class StringToDoubleConverter implements Converter<String, Double> {
    @Override
    public Double convert(String source) {
        return Double.parseDouble(source);
    }
}
