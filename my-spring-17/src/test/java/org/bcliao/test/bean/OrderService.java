package org.bcliao.test.bean;


import org.bcliao.beans.factory.annotation.Autowired;
import org.bcliao.stereotype.Component;

/**
 * @author bcliao
 * @date 2022/2/18 13:58
 */
@Component
public class OrderService {

    public OrderService(){
        System.out.println("OrderService---construct---");
    }

    @Autowired
    private IUserService userService;

    @Autowired
    private ProductService productService;

    public String query(String uId){
        return "order调用:  " + productService.query(uId);
    }
}
