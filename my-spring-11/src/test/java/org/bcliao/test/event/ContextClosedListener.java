package org.bcliao.test.event;

import org.bcliao.context.ApplicationListener;
import org.bcliao.context.event.ContextClosedEvent;

/**
 * @author bcliao
 * @date 2022/1/21 13:11
 */
public class ContextClosedListener implements ApplicationListener<ContextClosedEvent> {
    @Override
    public void onApplicationEvent(ContextClosedEvent event) {
        System.out.println("容器关闭事件：" + this.getClass().getName());
    }
}
