package org.bcliao.test.bean;

import org.bcliao.context.annotation.Scope;
import org.bcliao.stereotype.Component;

/**
 * @author bcliao
 * @date 2022/1/29 11:24
 */
@Component
@Scope("prototype")
public class User {

}
