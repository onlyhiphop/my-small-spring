package org.bcliao.context.support;

import org.bcliao.beans.factory.FactoryBean;
import org.bcliao.beans.factory.InitializingBean;
import org.bcliao.core.convert.ConversionService;
import org.bcliao.core.convert.converter.Converter;
import org.bcliao.core.convert.converter.ConverterFactory;
import org.bcliao.core.convert.converter.ConverterRegistry;
import org.bcliao.core.convert.converter.GenericConverter;
import org.bcliao.core.convert.support.DefaultConversionService;
import org.bcliao.core.convert.support.GenericConversionService;

import java.util.Set;

/**
 * @author bcliao
 * @date 2022/3/10 11:34
 */
public class ConversionServiceFactoryBean implements FactoryBean<ConversionService>, InitializingBean {

    private Set<?> converters;

    private GenericConversionService conversionService;

    @Override
    public ConversionService getObject() throws Exception {
        return conversionService;
    }

    @Override
    public Class<?> getObjectType() {
        return conversionService.getClass();
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        this.conversionService = new DefaultConversionService();
        registerConverters(converters, conversionService);
    }

    private void registerConverters(Set<?> converters, ConverterRegistry registry) {
        if(converters != null){
            for (Object converter : converters) {
                if(converter instanceof GenericConverter){
                    registry.addConverter((GenericConverter) converter);
                }
                else if(converter instanceof Converter<?, ?>){
                    registry.addConverter((Converter<?, ?>) converter);
                }
                else if(converter instanceof ConverterFactory<?, ?>){
                    registry.addConverterFactory((ConverterFactory<?, ?>) converter);
                }
                else{
                    throw new IllegalArgumentException("Each converter object must implement one of the " +
                            "Converter, ConverterFactory, or GenericConverter interfaces");
                }
            }
        }
    }


    public void setConverters(Set<?> converters) {
        this.converters = converters;
    }
}
