package org.bcliao.context.event;

import org.bcliao.context.ApplicationEvent;
import org.bcliao.context.ApplicationListener;

/**
 * @author bcliao
 * @date 2022/1/20 16:19
 */
public interface ApplicationEventMulticaster {

    /**
     * 添加监听器
     */
    void addApplicationListener(ApplicationListener<?> listener);

    /**
     * 删除监听器
     */
    void removeApplicationListener(ApplicationListener<?> listener);

    /**
     * 广播ApplicationEvent
     */
    void multicastEvent(ApplicationEvent event);
}
