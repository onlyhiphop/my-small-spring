package org.bcliao.example1;

import org.bcliao.bean.UserService;
import org.junit.Test;

/**
 * @author bcliao
 * @date 2021/12/20 10:35
 */
public class BeanFactory1Test {

    @Test
    public void test_BeanFactory1(){
        // 1.初始化 BeanFactory
        BeanFactory1 beanFactory = new BeanFactory1();

        // 2.注入bean
        beanFactory.registerBeanDefinition("userService", new UserService());

        // 3.获取bean
        UserService userService = (UserService) beanFactory.getBean("userService");
        userService.findUserInfo();
    }
}
