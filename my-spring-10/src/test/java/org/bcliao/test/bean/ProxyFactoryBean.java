package org.bcliao.test.bean;

import org.bcliao.beans.factory.FactoryBean;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

/**
 * @author bcliao
 * @date 2022/1/18 21:35
 */
public class ProxyFactoryBean implements FactoryBean<IUserDao> {
    @Override
    public IUserDao getObject() throws Exception {
        // 使用jdk的动态代理，生成 IUserDao的实现类

        InvocationHandler handler = (proxy, method, args) -> {
            if("queryUserName".equals(method.getName())){
                Map<String, String> maps = new HashMap<>();
                maps.put("001", "aaa");
                maps.put("002", "bbb");
                maps.put("003", "ccc");
                return maps.get(args[0].toString());
            }
            return null;
        };

        return (IUserDao) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{IUserDao.class}, handler);
    }

    @Override
    public Class<?> getObjectType() {
        return IUserDao.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
