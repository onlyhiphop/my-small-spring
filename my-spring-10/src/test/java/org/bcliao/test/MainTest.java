package org.bcliao.test;

import org.bcliao.context.support.ClassPathXmlApplicationContext;
import org.bcliao.test.bean.IUserDao;
import org.bcliao.test.bean.User;
import org.bcliao.test.bean.UserService;
import org.junit.Test;

/**
 * @author bcliao
 * @date 2022/1/16 16:34
 */
public class MainTest {


    @Test
    public void test_prototype(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring.xml");
        // 原型测试
        User user1 = (User) context.getBean("user");
        User user2 = (User) context.getBean("user");
        System.out.println(user1 == user2);

        //单例测试
        UserService userService1 = (UserService) context.getBean("userService");
        UserService userService2 = (UserService) context.getBean("userService");
        System.out.println(userService1 == userService2);

        //关闭时 只执行单例销毁
        context.close();
    }

    @Test
    public void test_FactoryBean(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring.xml");

        //获取FactoryBean
        Object userDaoFactoryBean = context.getBean("&userDao");
        //获取FactoryBean创建的bean
        IUserDao userDao = (IUserDao) context.getBean("userDao");

        System.out.println(userDaoFactoryBean.getClass().getName());
        System.out.println(userDao.getClass().getName());

        UserService userService = (UserService) context.getBean("userService");
        System.out.println(userService.queryUserInfo("001"));
    }

    @Test
    public void test_BeanPostProcessor(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring.xml");
    }
}