#### Spring 循环依赖问题
---

##### 前言

循环依赖主要分为这三种
![image1](img/Snipaste_2022-02-18_15-20-06.png)


##### 一、一级缓存

尝试使用一级缓存解决循环依赖问题

错误原因：因为 `AbstractAutowireCapableBeanFactory#applyPropertyValues` 注入对象属性（`BeanReference`）时又会去执行 `getBean`，导致一直递归死循环。

思路：会出现这种情况就是因为，我们每次都是等属性注入完之后，才返回这个对象，才放入 单例容器 中，也就是说会形成这样一个环就是因为在 单例容器 中获取不到对象。所以我们在对象实例化后，属性注入之前，就将该对象注册到 单例容器 中，等再次属性注入遇到该对象时，从单例容器中拿到半成品对象。

eg：`getBean("A")`时先去单例容器中获取，没有的话就执行 `createBean` 在实例化后，注入属性之前，将 A  放入 单例容器 中， 然后再 A 进行属性注入时遇到了 B ，同样的会执行 `getBean("B")` 跟 A 的流程一致，在 B 进行属性注入时，遇到了 A ，这时又执行 `getBean("A")` 但是这时单例容器中已经有了 A ，所以它不行再要执行 `createBean` ，这样 B 就成功注入了 A 该流程返回，回到最底层 A 也成功注入了 B 。

具体代码改动看git提交

衍生问题：发现 AOP 失效了
原因：AOP 主要实现在 `DefaultAdvisorAutoProxyCreator#postProcessAfterInitialization` 中，它是在Bean初始化（包含实例化、属性注入、initMethod执行）完成后执行，直接返回了生成的代理对象（对象改变了），比如 开始我们生成的是 A ，后来生成代理对象是 A' ，但是由于我们将放入单例容器操作提前了，所以我们从容器中拿出来的是 A ，而不是代理对象 A' ，所以 AOP 功能失效了。


##### 二、二级缓存

尝试使用二级缓存解决 AOP代理对象 问题

错误原因：由于我们将 注册对象到单例容器`registerSingleton` 的操作提前到了 对象实例化后 `createBeanInstance` 立即放入，这样的话，那单例容器中放入的只是 被代理对象 ，那我们在执行 `getBean` 时拿出来的是 被代理对象 ，我们知道要 AOP 生效，我们必须使用 代理对象 才行。

思路：`registerSingleton` 还是回到一开始的位置，在所有操作（实例化、属性注入、初始化）之后执行，我们再建立一个 二级缓存 `earlySingletonObjects` 用来存放半成品对象，在 `getSingleton` 时，先从 `singletonObjects`单例容器中获取，如果没有就从 `earlySingletonObjects` 中取。这样我们保证了 `singletonObjects` 中取出来的都是成品对象，`earlySingletonObjects`取出来都是半成品对象。

eg：假设 A 要实现 AOP 代理，依赖关系( A -> B -> A)，且 A、B 都是非惰性（就是容器加载的时候就会创建）
，A 在 `createBeanInstance` 时立即将半成品 A 放入了 `earlySingletonObjects` ，当进行属性注入时，碰到了 B ，那么 B 又会在 `createBeanInstance` 时将半成品 B 放入 `earlySingletonObjects`，此时 B 中有 A ，这时执行 `getSingleton` 在 `singletonObjects` 中没有取到，于是从 `earlySingletonObjects` 中取，拿到了半成品的 A ，所以 B 创建完成 完成品放入 `singletonObjects` ，最后返回到 A ，A也从拿到了完成品B，至此 A 完成流程，完成品 A （执行完了流程 A 替换成了代理对象）放入 `singletonObjects` 。此时单例容器中 A 是代理对象，所以 AOP 是生效的。
我们可以加上当完成品放入了 `singletonObjects` 我们就将半成品从 `earlySingletonObjects` 删除。

具体代码改动看git提交

问题：A -> B -> A 中 B 是需要被代理对象， A 中执行 B 中的代理发现 AOP 失效
原因：这里是有先后顺序的关系，假设容器先加载 B ，那么 A 在属性注入 B 时，已经能在 `singletonObjects` 拿到成品 B ，此时这个 B 是代理对象。那么 A 调用 B ，此时 AOP 是成功的。 但是如果容器先加载 A ，那么 A 在属性注入 B 时，在 `singletonObjects`中没有成品 B ，就在 `earlySingletonObjects` 取到了半成品 B ，而半成品的 B 是还没有被AOP代理的，所以最后 A 调用 B ，此时 AOP 发现是失效的。


> 有什么办法解决这个生成代理对象的问题？
思路：就提前生成代理对象直接将其放入二级缓存？ 可以将生成代理的过程保存下来，当需要使用时，就直接执行 创建过程 来获取代理对象，这样使用函数式编程思想，就可以复用之前写好的创建AOP过程，就不需要同一段逻辑代码写两次。

实现：创建 `ObjectFactory` 函数接口保存 生成代理对象过程 ， `getObject` 方法返回代理对象；二级缓存 `singletonFactories` Map中value存 `ObjectFactory` ；在实例化 `createBeanInstance` 之后，将创建AOP代理对象操作存入二级缓存 `singletonFactories` ，在 `getSingleton`  时，先从 `singletonObjects` 以及缓存中取，没有从取二级缓存 `singletonFactories`取出 `ObjectFactory` 执行其 `getObject` 方法就获取到了代理对象。这里需要注意的是，由于我们将生成代理提前了，为了防止后续重复执行创建代理对象操作，可以加个缓存存下已经创建过的代理 `DefaultAdvisorAutoProxyCreator#earlyProxyReferences`。


##### 三级缓存
前面分析可知，只需要一个单例容器缓存成品对象，二级缓存缓存创建代理操作，已经可以解决AOP循环依赖问题，那为啥还要三级缓存呢？
1、避免重复执行 `ObjectFactory#getObject`
2、为了解耦，遵循单一责任原则，一个缓存只做一件事，方便后期维护和扩展，减少由于不同阶段的对象混杂在一起而出现的各种奇怪的问题。

上述中 `DefaultAdvisorAutoProxyCreator#earlyProxyReferences` 只是缓存了已经创建的代理对象，假若这个对象不需要代理，那是不是没有地方去存一个正在循环依赖中且不需要代理的半成品对象？
1、这样在我们 `getSingleton` 中，如果没有找到该半成品对象，又会去执行 `ObjectFactory#getObject` ,虽然里面做了判断不需要代理就直接返回，不会重复去创建对象，但是这样的代码耦合太高,对于后续扩展不好，一旦在 `getObject` 忽略了该判断，那代码就很可能出现问题，因为里面混杂了两种情况（取代理对象，取不需要代理的半成品对象）。所以增加二级缓存，用来缓存半成品对象，三级缓存创建代理操作。这样，在 `getSingleton`中同一个对象的 `ObjectFactory#getObject` 只会执行一次，因为如果已经执行过的已经放入了 二级缓存 变成了半成品对象。
2、之前我们在 `getSingleton` 中执行完 `ObjectFactory#getObject` 就直接放入了一级缓存，这样会导致半成品对象，也会进入一级缓存，因为执行 `getSingleton` 是会存在循环依赖过程中。

三级缓存具体代码改动看git提交


推荐网上对此讲解比较好的一篇文章：
> https://zhuanlan.zhihu.com/p/342204687


##### 总结
如果只是要解决循环依赖的问题，那么一级缓存就一级足够了，但是它无法解决 Spring 中 AOP 带来的问题；如果要解决 Spring 中 AOP 的问题，其中只需要两个缓存就足够，但是两个缓存会使代码逻辑不仅耦合度高，还会出现代码同一段创建对象逻辑重复执行的问题；最后使用三级缓存是为了设计上的合理性和扩展性，将代码解耦，每个缓存实现单一责任，三个缓存存放不同时期的对象。
