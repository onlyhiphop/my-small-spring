package org.bcliao.context.annotation;

import cn.hutool.core.util.ClassUtil;
import org.bcliao.beans.factory.config.BeanDefinition;
import org.bcliao.stereotype.Component;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * A component provider that scans the classpath from a base package. It then
 *  applies exclude and include filters to the resulting classes to find candidates.
 *
 * @author bcliao
 * @date 2022/1/28 14:55
 */
public class ClassPathScanningCandidateComponentProvider {

    public Set<BeanDefinition> findCandidateComponents(String basePackage){
        Set<BeanDefinition> candidate = new LinkedHashSet<>();
        Set<Class<?>> classes = ClassUtil.scanPackageByAnnotation(basePackage, Component.class);
        for (Class<?> clazz : classes) {
            candidate.add(new BeanDefinition(clazz));
        }
        return candidate;
    }

}
