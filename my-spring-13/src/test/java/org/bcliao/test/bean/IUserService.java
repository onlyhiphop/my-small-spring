package org.bcliao.test.bean;

/**
 * @author bcliao
 * @date 2022/1/22 15:42
 */
public interface IUserService {

    String queryUserInfo();

    String register(String userName);

}
