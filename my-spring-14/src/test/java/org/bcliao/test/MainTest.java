package org.bcliao.test;

import org.bcliao.context.support.ClassPathXmlApplicationContext;
import org.bcliao.test.bean.User;
import org.bcliao.test.bean.UserService;
import org.junit.Test;

/**
 * @author bcliao
 * @date 2022/1/28 14:32
 */
public class MainTest {

    @Test
    public void test_scan_component(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring.xml");
        UserService userService = (UserService) context.getBean("userService");
        userService.queryUserInfo();
    }

    @Test
    public void test_scan_scope(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring.xml");
        User user1 = (User) context.getBean("user");
        User user2 = (User) context.getBean("user");
        System.out.println(user1 == user2);
    }

    @Test
    public void test_PropertyPlaceholder(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring-property.xml");
        UserService userService = (UserService) context.getBean("userService");
        String token = userService.getToken();
        System.out.println(token);
    }

}
