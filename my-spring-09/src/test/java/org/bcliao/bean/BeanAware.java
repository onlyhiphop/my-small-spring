package org.bcliao.bean;

import org.bcliao.beans.BeansException;
import org.bcliao.beans.factory.BeanClassLoaderAware;
import org.bcliao.beans.factory.BeanFactory;
import org.bcliao.beans.factory.BeanFactoryAware;
import org.bcliao.beans.factory.BeanNameAware;
import org.bcliao.context.ApplicationContext;
import org.bcliao.context.ApplicationContextAware;

/**
 * @author bcliao
 * @date 2022/1/14 15:54
 */
public class BeanAware implements BeanFactoryAware, BeanNameAware, BeanClassLoaderAware, ApplicationContextAware {

    private String beanName;
    private ClassLoader classLoader;
    private BeanFactory beanFactory;
    private ApplicationContext applicationContext;

    @Override
    public void setBeanClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }

    @Override
    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public String toString() {
        return "BeanAware{" +
                "beanName='" + beanName + '\'' +
                ", classLoader=" + classLoader +
                ", beanFactory=" + beanFactory +
                ", applicationContext=" + applicationContext +
                '}';
    }
}
