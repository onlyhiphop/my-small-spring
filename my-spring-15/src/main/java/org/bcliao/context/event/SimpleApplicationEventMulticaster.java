package org.bcliao.context.event;

import org.bcliao.beans.factory.BeanFactory;
import org.bcliao.context.ApplicationEvent;
import org.bcliao.context.ApplicationListener;

/**
 * @author bcliao
 * @date 2022/1/21 9:55
 */
public class SimpleApplicationEventMulticaster extends AbstractApplicationEventMulticaster{

    public SimpleApplicationEventMulticaster(BeanFactory beanFactory){
        setBeanFactory(beanFactory);
    }

    @Override
    public void multicastEvent(ApplicationEvent event) {
        for (ApplicationListener<ApplicationEvent> listener : getApplicationListeners(event)) {
            listener.onApplicationEvent(event);
        }
    }
}
