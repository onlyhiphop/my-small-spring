package org.bcliao.factory.support;

import org.bcliao.factory.config.BeanDefinition;

public interface BeanDefinitionRegistry {

    /**
     * 向注册表里注册 BeanDefinition
     */
    void registerBeanDefinition(String beanName, BeanDefinition beanDefinition);
}
