package org.bcliao.aop;

import org.bcliao.util.ClassUtils;

/**
 * 被代理对象
 * @author bcliao
 * @date 2022/1/22 22:22
 */
public class TargetSource {

    private final Object target;

    public TargetSource(Object target) {
        this.target = target;
    }

    public Class<?>[] getTargetClass(){
        // 这个 target 可能是JDK代理创建，也可能是cglib创建，为了保证正确获取到结果，需要增加判读
        Class<?> clazz = this.target.getClass();
        clazz = ClassUtils.isCglibProxyClass(clazz) ? clazz.getSuperclass() : clazz;
        return clazz.getInterfaces();
    }

    public Object getTarget(){
        return this.target;
    }
}
