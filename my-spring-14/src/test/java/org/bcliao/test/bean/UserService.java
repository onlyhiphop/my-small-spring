package org.bcliao.test.bean;

import org.bcliao.stereotype.Component;

/**
 * @author bcliao
 * @date 2022/1/29 9:42
 */
@Component("userService")
public class UserService {

    private String token;

    public void queryUserInfo(){
        System.out.println("查询用户信息");
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
