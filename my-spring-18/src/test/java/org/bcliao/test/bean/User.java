package org.bcliao.test.bean;

import org.bcliao.beans.factory.annotation.Value;
import org.bcliao.stereotype.Component;

import java.time.LocalDate;

/**
 * @author bcliao
 * @date 2022/3/11 13:48
 */
@Component
public class User {
    @Value("bcliao")
    private String name;
    @Value("1998/03/17")
    private LocalDate birthday;
    @Value("0.005")
    private Double assets;

    public Double getAssets() {
        return assets;
    }

    public void setAssets(Double assets) {
        this.assets = assets;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", birthday=" + birthday +
                ", assets=" + assets +
                '}';
    }
}
