package org.bcliao.test.bean;

import org.bcliao.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author bcliao
 * @date 2022/2/14 11:38
 */
@Component
public class UserDao {

    private final static Map<String, String> maps = new HashMap<String, String>(){{
        put("001", "AAA");
        put("002", "BBB");
        put("003", "CCC");
    }};

    public String queryUserName(String uId){
        return maps.get(uId);
    }
}
