package org.bcliao.beans.factory.support;

import cn.hutool.core.bean.BeanUtil;
import org.bcliao.beans.BeansException;
import org.bcliao.beans.PropertyValue;
import org.bcliao.beans.PropertyValues;
import org.bcliao.beans.factory.config.AutowireCapableBeanFactory;
import org.bcliao.beans.factory.config.BeanDefinition;
import org.bcliao.beans.factory.config.BeanReference;

import java.lang.reflect.Constructor;

/**
 * @author bcliao
 * @date 2021/12/22 9:30
 */
public abstract class AbstractAutowireCapableBeanFactory extends AbstractBeanFactory implements AutowireCapableBeanFactory {

    /**
     * 默认使用 cglib 策略
     */
    private InstantiationStrategy instantiationStrategy = new CglibSubclassingInstantiationStrategy();

    @Override
    protected Object createBean(String beanName, BeanDefinition beanDefinition, Object[] args) throws BeansException {

        Object bean;
        try {
            bean = createBeanInstance(beanName, beanDefinition, args);
            //注入属性
            applyPropertyValues(beanName, bean, beanDefinition);
        } catch (Exception e) {
            throw new BeansException("Instantiation of bean failed", e);
        }
        // 创建后，放入单例容器中
        registerSingletion(beanName, bean);
        return bean;
    }

    protected void applyPropertyValues(String beanName, Object bean, BeanDefinition beanDefinition) {
        try {
            PropertyValues propertyValues = beanDefinition.getPropertyValues();
            for (PropertyValue p :
                    propertyValues.getPropertyValues()) {
                String name = p.getBeanName();
                Object value = p.getValue();

                //注入对象
                if(value instanceof BeanReference){
                    BeanReference beanReference = (BeanReference) value;
                    value = getBean(beanReference.getBeanName());
                }

                //属性填充
                BeanUtil.setFieldValue(bean, name, value);
            }
        } catch (Exception ex) {
            throw new BeansException("Error setting property values：" + beanName);
        }
    }


    protected Object createBeanInstance(String beanName, BeanDefinition beanDefinition, Object[] args) {
        // 通过参数列表判断使用哪个构造器
        Constructor<?> constructorUse = null;
        Class<?> beanClass = beanDefinition.getBeanClass();
        Constructor<?>[] declaredConstructors = beanClass.getDeclaredConstructors();
        for (Constructor c :
                declaredConstructors) {
            if (args != null && c.getParameterCount() == args.length) {
                constructorUse = c;
                break;
            }
        }
        return getInstantiationStrategy().instantiate(beanDefinition, beanName, constructorUse, args);
    }

    public InstantiationStrategy getInstantiationStrategy() {
        return instantiationStrategy;
    }

    public void setInstantiationStrategy(InstantiationStrategy instantiationStrategy) {
        this.instantiationStrategy = instantiationStrategy;
    }
}
