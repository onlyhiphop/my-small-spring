package org.bcliao.test.event;

/**
 * @author bcliao
 * @date 2022/1/16 16:35
 */
public class User {

    private String uId;
    private String userName;

    public void destroyMethod(){
        System.out.println("User 执行了销毁方法--------");
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
