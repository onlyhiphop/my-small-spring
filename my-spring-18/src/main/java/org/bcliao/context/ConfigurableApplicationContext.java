package org.bcliao.context;

import org.bcliao.beans.BeansException;
import org.bcliao.beans.factory.config.BeanFactoryPostProcessor;
import org.bcliao.core.convert.ConversionService;

/**
 * @author bcliao
 * @date 2022/1/4 10:29
 */
public interface ConfigurableApplicationContext extends ApplicationContext {

    /**
     * Name of the ConversionService bean in the factory.
     * If none is supplied, default conversion rules apply.
     * @since 3.0
     * @see ConversionService
     */
    String CONVERSION_SERVICE_BEAN_NAME = "conversionService";

    /**
     * 加载或刷新容器配置
     */
    void refresh() throws BeansException;

    /**
     * 手动添加 BeanFactoryPostProcessor
     */
    void addBeanFactoryPostProcessor(BeanFactoryPostProcessor postProcessor);

    /**
     * 注册一个JVM关闭钩子
     */
    void registerShutdownHook();

    /**
     * 手动关闭资源
     */
    void close();
}
