package org.bcliao.beans.factory.support;

import cn.hutool.core.lang.Assert;
import org.bcliao.beans.BeansException;
import org.bcliao.beans.factory.BeanFactory;
import org.bcliao.beans.factory.FactoryBean;
import org.bcliao.beans.factory.config.BeanDefinition;
import org.bcliao.beans.factory.config.BeanPostProcessor;
import org.bcliao.beans.factory.config.ConfigurableBeanFactory;
import org.bcliao.core.convert.ConversionService;
import org.bcliao.util.ClassUtils;
import org.bcliao.util.StringValueResolver;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author bcliao
 * @date 2021/12/21 10:55
 */
public abstract class AbstractBeanFactory extends FactoryBeanRegistrySupport implements ConfigurableBeanFactory {

    /**
     * 父bean工厂，用于bean继承支持
     */
    private BeanFactory parentBeanFactory;

    private final List<BeanPostProcessor> beanPostProcessors = new CopyOnWriteArrayList<>();

    private ClassLoader beanClassLoader = ClassUtils.getDefaultClassLoader();

    /**
     * 字符串解析器用于注释属性的值
     */
    private final List<StringValueResolver> embeddedValueResolvers = new CopyOnWriteArrayList<>();

    private ConversionService conversionService;

    @Override
    public Object getBean(String name) throws BeansException {
        return doGetBean(name, null);
    }

    @Override
    public Object getBean(String name, Object... args) throws BeansException {
        return doGetBean(name, args);
    }

    @Override
    public <T> T getBean(String name, Class<T> requiredType) throws BeansException {
        return (T) getBean(name);
    }


    protected <T> T doGetBean(String name, Object[] args){

        // 去除 '&' 开头
        String beanName = name;
        if(name.startsWith(BeanFactory.FACTORY_BEAN_PREFIX)){
            beanName = name.substring(BeanFactory.FACTORY_BEAN_PREFIX.length());
        }

        //尝试从单例容器缓存中获取
        Object sharedInstance = getSingleton(beanName);
        if(sharedInstance != null){
            return (T) getObjectForBeanInstance(sharedInstance, name, beanName);
        }
        //如果单例容器中不存在，就拿到 BeanDefinition 自己创建
        BeanDefinition beanDefinition = getBeanDefinition(beanName);
        Object bean = createBean(beanName, beanDefinition, args);
        return (T) getObjectForBeanInstance(bean, name, beanName);
    }

    protected Object getObjectForBeanInstance(
            Object beanInstance, String name, String beanName){
        //对于FactoryBean不处理
        if(!(beanInstance instanceof FactoryBean)){
            return beanInstance;
        }
        // "&" 开头，直接获取 FactoryBean
        if(name.startsWith(BeanFactory.FACTORY_BEAN_PREFIX)){
            return beanInstance;
        }
        // 从FactoryBean的单例容器中获取
        Object bean = getCachedObjectForFactoryBean(beanName);
        if(bean == null){
            FactoryBean<?> factoryBean = (FactoryBean<?>) beanInstance;
            bean = getObjectFromFactoryBean(factoryBean, beanName);
        }
        return bean;
    }

    protected abstract BeanDefinition getBeanDefinition(String beanName) throws BeansException;

    protected abstract Object createBean(String beanName, BeanDefinition beanDefinition, Object[] args) throws BeansException;

    @Override
    public BeanFactory getParentBeanFactory() {
        return this.parentBeanFactory;
    }

    @Override
    public void addBeanPostProcessor(BeanPostProcessor beanPostProcessor) {
        Assert.notNull(beanPostProcessor, "BeanPostProcessor must not be null");
        //如果已经存在，移除
        this.beanPostProcessors.remove(beanPostProcessor);
        this.beanPostProcessors.add(beanPostProcessor);
    }

    /**
     * @return 返回所有 BeanPostProcessors
     */
    public List<BeanPostProcessor> getBeanPostProcessors(){
        return this.beanPostProcessors;
    }

    public ClassLoader getBeanClassLoader() {
        return beanClassLoader;
    }

    @Override
    public void addEmbeddedValueResolver(StringValueResolver valueResolver) {
        Assert.notNull(valueResolver, "StringValueResolver must not be null");
        this.embeddedValueResolvers.add(valueResolver);
    }

    @Override
    public String resolveEmbeddedValue(String value) {
        if(value == null){
            return null;
        }
        String result = value;
        for (StringValueResolver resolver : this.embeddedValueResolvers) {
            result = resolver.resolveStringValue(result);
            if(result == null){
                return null;
            }
        }
        return result;
    }

    @Override
    public boolean containsBean(String name) {
        return containsBeanDefinition(name);
    }

    protected abstract boolean containsBeanDefinition(String beanName);

    @Override
    public void setConversionService(ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    public ConversionService getConversionService() {
        return conversionService;
    }
}