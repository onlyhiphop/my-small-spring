package org.bcliao.factory.support;

import org.bcliao.factory.BeansException;
import org.bcliao.factory.config.BeanDefinition;

import java.lang.reflect.Constructor;

/**
 * @author bcliao
 * @date 2021/12/22 9:30
 */
public abstract class AbstractAutowireCapableBeanFactory extends AbstractBeanFactory {

    /**
     *  默认使用 cglib 策略
     */
    private InstantiationStrategy instantiationStrategy = new CglibSubclassingInstantiationStrategy();

    @Override
    protected Object createBean(String beanName, BeanDefinition beanDefinition, Object[] args) throws BeansException {

        Object bean;
        try {
            bean = createBeanInstance(beanName, beanDefinition, args);
        } catch (Exception e) {
            throw new BeansException("Instantiation of bean failed", e);
        }
        // 创建后，放入单例容器中
        registerSingletion(beanName, bean);
        return bean;
    }

    protected Object createBeanInstance(String beanName, BeanDefinition beanDefinition, Object[] args){
        // 通过参数列表判断使用哪个构造器
        Constructor<?> constructorUse = null;
        Class<?> beanClass = beanDefinition.getBeanClass();
        Constructor<?>[] declaredConstructors = beanClass.getDeclaredConstructors();
        for (Constructor c :
                declaredConstructors) {
            if (args != null && c.getParameterCount() == args.length) {
                constructorUse = c;
                break;
            }
        }
        return getInstantiationStrategy().instantiate(beanDefinition, beanName, constructorUse, args);
    }

    public InstantiationStrategy getInstantiationStrategy() {
        return instantiationStrategy;
    }

    public void setInstantiationStrategy(InstantiationStrategy instantiationStrategy) {
        this.instantiationStrategy = instantiationStrategy;
    }
}
