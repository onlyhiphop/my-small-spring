package org.bcliao.test.event;

import org.bcliao.context.ApplicationListener;

/**
 * @author bcliao
 * @date 2022/1/21 11:24
 */
public class CustomListener implements ApplicationListener<CustomEvent> {
    @Override
    public void onApplicationEvent(CustomEvent event) {
        System.out.println(event.getMessage());
    }
}
