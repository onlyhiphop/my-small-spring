package org.bcliao.core.convert;

/**
 * 类型转换抽象接口
 * @author bcliao
 * @date 2022/3/8 10:35
 */
public interface ConversionService {

    boolean canConvert(Class<?> sourceType, Class<?> targetType);

    <T> T convert(Object source, Class<?> targetType);
}
