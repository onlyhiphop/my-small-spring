package org.bcliao.test;

import org.bcliao.context.support.ClassPathXmlApplicationContext;
import org.bcliao.test.bean.IUserService;
import org.junit.Test;

/**
 * @author bcliao
 * @date 2022/2/14 11:07
 */
public class MainTest {

    @Test
    public void test_autoProxy(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring.xml");
        IUserService userService = context.getBean("userService", IUserService.class);
        System.out.println(userService.queryUserName("002"));;

        IUserService userService2 = context.getBean("userService", IUserService.class);
        System.out.println(userService == userService2);
    }

}
