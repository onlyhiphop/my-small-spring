package org.bcliao.test.event;

import org.bcliao.context.ApplicationListener;
import org.bcliao.context.event.ContextRefreshedEvent;

/**
 * @author bcliao
 * @date 2022/1/21 13:10
 */
public class ContextRefreshedListener implements ApplicationListener<ContextRefreshedEvent> {
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        System.out.println("容器刷新完毕事件：" + this.getClass().getName());
    }
}
