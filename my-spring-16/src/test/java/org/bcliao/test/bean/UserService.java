package org.bcliao.test.bean;

import org.bcliao.beans.factory.annotation.Autowired;
import org.bcliao.stereotype.Component;

/**
 * @author bcliao
 * @date 2022/2/14 11:35
 */
@Component
public class UserService implements IUserService{

    @Autowired
    private UserDao userDao;

    @Override
    public String queryUserName(String uId) {
        return userDao.queryUserName(uId);
    }
}
