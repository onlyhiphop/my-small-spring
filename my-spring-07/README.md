#### IOC

----



##### 目标

1、ApplicationContext

![image-20220104093438621](img/image-20220104093438621.png)

该两步过程还略显复杂，还可以再包一层再暴露给用户使用。

还可以在这个过程中加入许多可扩展的步骤（如本章第二点就是其中一个）



2、扩展接口 BeanPostProcessor、 BeanFactoryPostProcessor

`BeanPostProcessor`：允许在 Bean 对象实例化之后修改 Bean 对象，也可以替换Bean对象 （与后面的AOP有密切关系）

`BeanFactoryPostProcessor`：允许在 Bean 对象注册后但未实例化之前，对Bean的定义信息 BeanDefinition 进行修改操作

要实现这两个接口，我们需要找到对应的初始化步骤（对应本章第一点）



##### 设计

1、ApplicationContext

​	回忆参考前面章节我们 BeanFactory 是如何设计的

​	1）顶层接口 `ApplicationContext` ，这里继承了 BeanFactory ，是因为它的定义就是继承BeanFactory的功能并扩充外加功能

​	2）将顶层接口拆分功能 创建子接口 `ConfigurableApplicationContext`

​	3）模板模式创建抽象类 `AbstractApplicationContext`

​	4）后面依次创建 `AbstractRefreshableApplicationContext` `AbstractXmlApplicationContext` 抽象类实现上一个抽象类的方法 （一层包一层，每层只实现部分功能，让每个类的功能都非常细）

​	5）最后实现类 `ClassPathXmlApplicationContext` 我们发现只需要关注给出  configLocations 资源的定位，其他的创建BeanFactory，加载BeanDefinition等一系列操作都在上层模板类中已经完成了，这样显得这个类很干净。



​	refresh() 方法就是我们整合的流程包含初始化工厂、加载BeanDefinition、扩展处理等。



2、BeanPostProcessor、 BeanFactoryPostProcessor

​	`BeanPostProcessor` 在Bean实例化完成且属性注入后执行。在 `refresh` 过程中的 `registerBeanPostProcessors`，已经将所有 实现了 `BeanPostProcessor` 接口的类放入了 `AbstractBeanFactory` 的存储List中

```java
private final List<BeanPostProcessor> beanPostProcessors = new CopyOnWriteArrayList<>();
```

​	再在 `createBean` 实例化Bean后遍历执行 BeanPostProcessor 处理器的方法



​	`BeanFactoryPostProcessor` 执行操作在 Bean 实例化之前，在 refresh 方法中 invokeBeanFactoryPostProcessors 。



##### 结构图

![image-20220104100153401](img/image-20220104100153401.png)


##### 测试
org.bcliao.test.MainTest




##### 总结

​	大家还是主要是理解它为什么这么设计，学习它的设计思维，我这里并没有写太多实现细节，都是从全局角度看设计。其实里面还有很多值得学习的实现细节我没有列出，所以还是建议一定要动手！动手！动手！跟着敲一遍，去感受其中的代码实现。

​	也可以将spring源码拷到本地，边写也可以边阅读源码（这里的代码命名都是和spring源码一致，所以可以直接搜索对应的类和方法）

