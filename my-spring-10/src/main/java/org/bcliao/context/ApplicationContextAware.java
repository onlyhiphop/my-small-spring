package org.bcliao.context;

import org.bcliao.beans.BeansException;
import org.bcliao.beans.factory.Aware;

/**
 * 实现此接口，就能感知到所属的 ApplicationContext
 * @author bcliao
 * @date 2022/1/14 10:34
 */
public interface ApplicationContextAware extends Aware {

    void setApplicationContext(ApplicationContext applicationContext) throws BeansException;
}
