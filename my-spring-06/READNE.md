#### IOC

---

​	这一章我们将 BeanFactory 部分重构，对齐spring源码



##### spring中的工厂类结构图

![image-20211228134024813](img/image-20211228134024813.png)

为什么要定义这么多层次的接口呢？

​	每个接口都有它的使用场合，也为了区分在 Spring 内部操作过程中对象的传递和转化，对对象的数据访问所做的限制。

1. `ListableBeanFactory` 表示这些 Bean 可列表化（根据各种条件获取配置清单）
2. `HierarchicalBeanFactory` 表示这些 Bean 是有继承关系的，有就是每个 Bean 可能有父类 Bean ，它提供可以获取父类 BeanFatory 方法
3. `AutowireCapableBeanFactory` 定义 Bean 的自动装配规则
4. `ConfigurableBeanFactory` 可以获取 `BeanPostProcessor`  `BeanClassLoader` 等的一个配置化接口
5. `ConfigurableListableBeanFactory` 提供分析和修改 Bean 以及预先实例化的操作接口



##### 总结

​	不用死记，也记不住，对着代码多感受几遍它的设计。总的来说这里也就是对 BeanFactory 的功能扩展细化，用不同的接口来区分不同的功能。















