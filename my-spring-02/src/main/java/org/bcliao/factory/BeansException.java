package org.bcliao.factory;

/**
 * @author bcliao
 * @date 2021/12/21 9:59
 */
public class BeansException extends RuntimeException{

    public BeansException(String msg) {
        super(msg);
    }

    public BeansException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
