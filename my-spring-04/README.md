IOC

---

​	本章添加功能：属性注入

​	本章加入在类实例化的时候，把属性信息填充，填充的不只是 int、Long、String，还包括没有实例化的对象属性（这里暂时不考虑Bean的循环依赖）

​	

##### 设计思路

​	要将属性注入，肯定是要在实例化操作之后，也就是在 `AbstractAutowireCapableBeanFactory` 的 createBean 方法里面，所以我们创建填充属性方法 applyPropertyValues ；填充属性，必须要获取到属性的 名称和值，之前我们说过 BeanDefinition 就是用来记录类信息的，所以我们应该在 BeanDefinition 中记录下该类的属性信息。

​	发挥OOP思想，将类的属性相关封装成一个类 `PropertyValues` （方便后续扩展）；单独一个属性又可以封装成 `PropertyValue` 

```java
public class PropertyValues {
    private final List<PropertyValue> propertyValueList;
}
```

​	A 中引用了B

​	由于属性的值可能是引用了别的对象，如果是那该怎么填充呢？我们肯定不能直接 new 赋值进去，我们要从spring容器取，也就是重复走一遍 getBean 方法。那我们怎么判断 B 是一个需要执行 genBean 方法的对象呢，那就需要再创建一个对象 `BeanReference` 来标识，那通过什么来关联到具体类呢？所以需要有  beanName 属性。

```java
public class BeanReference {
    private final String beanName;
}
```



##### 总结

​	本章又扩充了 `AbstractAutowireCapableBeanFactory` 类注入属性功能，同样的我们在扩充新功能的同时，并没有改动我们的具体使用类 `DefaultListableBeanFactory` ，大家可以多捋几遍感受一下代码设计带来的魅力。

​	本章最大的感触应是OOP面向对象的思想，将需要注入属性封装成类 `PropertyValues`，将需要注入的类封装成 `BeanReference`。

