package org.bcliao.beans.factory;

/**
 * 标记类接口，实现该接口可以被Spring容器感知
 * @author bcliao
 * @date 2022/1/14 10:12
 */
public interface Aware {
}
