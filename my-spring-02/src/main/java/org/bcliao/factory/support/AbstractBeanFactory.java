package org.bcliao.factory.support;

import org.bcliao.factory.BeanFactory;
import org.bcliao.factory.BeansException;
import org.bcliao.factory.config.BeanDefinition;

/**
 * @author bcliao
 * @date 2021/12/21 10:55
 */
public abstract class AbstractBeanFactory extends DefaultSingletonBeanRegistry implements BeanFactory {

    @Override
    public Object getBean(String beanName) throws BeansException {
        //尝试从单例容器缓存中获取
        Object singleton = getSingleton(beanName);
        if(singleton != null){
            return singleton;
        }
        //如果单例容器中不存在，就拿到 BeanDefinition 自己创建
        BeanDefinition beanDefinition = getBeanDefinition(beanName);
        return createBean(beanName, beanDefinition);
    }

    protected abstract BeanDefinition getBeanDefinition(String beanName) throws BeansException;

    protected abstract Object createBean(String beanName, BeanDefinition beanDefinition) throws BeansException;

}
