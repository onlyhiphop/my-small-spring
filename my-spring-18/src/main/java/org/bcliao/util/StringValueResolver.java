package org.bcliao.util;

/**
 * Simple strategy interface for resolving a String value.
 * Used by {@link org.bcliao.beans.factory.config.ConfigurableBeanFactory}.
 * @author bcliao
 * @date 2022/2/9 11:27
 */
public interface StringValueResolver {

    String resolveStringValue(String strVal);

}
