package org.bcliao.context.support;

import cn.hutool.core.lang.Assert;
import org.bcliao.beans.BeansException;
import org.bcliao.beans.factory.BeanFactory;
import org.bcliao.beans.factory.ConfigurableListableBeanFactory;
import org.bcliao.beans.factory.config.BeanFactoryPostProcessor;
import org.bcliao.beans.factory.config.BeanPostProcessor;
import org.bcliao.context.ApplicationEvent;
import org.bcliao.context.ApplicationListener;
import org.bcliao.context.ConfigurableApplicationContext;
import org.bcliao.context.event.ApplicationEventMulticaster;
import org.bcliao.context.event.ContextClosedEvent;
import org.bcliao.context.event.ContextRefreshedEvent;
import org.bcliao.context.event.SimpleApplicationEventMulticaster;
import org.bcliao.core.io.DefaultResourceLoader;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author bcliao
 * @date 2022/1/4 10:46
 */
public abstract class AbstractApplicationContext extends DefaultResourceLoader implements ConfigurableApplicationContext {

    /** JVM关闭钩子 */
    private Thread shutdownHook;
    /** 直接在容器中注册的 BeanFactoryPostProcessor. */
    private final List<BeanFactoryPostProcessor> beanFactoryPostProcessors = new ArrayList<>();

    private static final String APPLICATION_EVENT_MULTICASTER_BEAN_NAME = "applicationEventMulticaster";
    /** 事件广播器 */
    private ApplicationEventMulticaster applicationEventMulticaster;

    @Override
    public void refresh() throws BeansException {
        //1. 创建 BeanFactory，并加载 BeanDefinition
        refreshBeanFactory();

        //2. 获取 BeanFactory
        ConfigurableListableBeanFactory beanFactory = getBeanFactory();

        //3. 添加后置处理器，让继承 ApplicationContextAware 的bean对象都能感知到所属 ApplicationContext
        beanFactory.addBeanPostProcessor(new ApplicationContextAwareProcessor(this));

        //4. 在 Bean 实例化之前执行 BeanFactoryPostProcessor
        invokeBeanFactoryPostProcessors(beanFactory);

        //5. 注册后置处理器 BeanPostProcessor  最后执行器在 Bean 对象实例化之前执行
        registerBeanPostProcessors(beanFactory);

        //6. 初始化事件广播器， 讲 name 为 applicationEventMulticaster的 bean 放入容器种
        initApplicationEventMulticaster();

        //7. 注册事件监听器
        registerListeners();

        //8. 提前实例化单例 Bean 对象
        beanFactory.preInstantiateSingletons();

        //9. 发容器去刷新完成事件
        finishRefresh();
    }

    private void initApplicationEventMulticaster(){
        ConfigurableListableBeanFactory beanFactory = getBeanFactory();
        this.applicationEventMulticaster = new SimpleApplicationEventMulticaster(beanFactory);
        beanFactory.registerSingletion(APPLICATION_EVENT_MULTICASTER_BEAN_NAME, this.applicationEventMulticaster);
    }

    private void registerListeners(){
        Collection<ApplicationListener> listeners = getBeansOfType(ApplicationListener.class).values();
        for (ApplicationListener listener : listeners) {
            applicationEventMulticaster.addApplicationListener(listener);
        }
    }

    private void finishRefresh(){
        publishEvent(new ContextRefreshedEvent(this));
    }

    @Override
    public void publishEvent(ApplicationEvent event) {
        applicationEventMulticaster.multicastEvent(event);
    }

    /**
     * 子类必须实现这个方法来执行实际的配置加载。
     */
    protected abstract void refreshBeanFactory() throws BeansException;

    protected abstract ConfigurableListableBeanFactory getBeanFactory();

    private void registerBeanPostProcessors(ConfigurableListableBeanFactory beanFactory) {
        Map<String, BeanPostProcessor> beanPostProcessorMap = beanFactory.getBeansOfType(BeanPostProcessor.class);
        for (BeanPostProcessor beanPostProcessor : beanPostProcessorMap.values()) {
            beanFactory.addBeanPostProcessor(beanPostProcessor);
        }
    }

    private void invokeBeanFactoryPostProcessors(ConfigurableListableBeanFactory beanFactory) {
        //1、先遍历执行已经在容器中手动注册的
        for (BeanFactoryPostProcessor beanFactoryPostProcessor : getBeanFactoryPostProcessors()) {
            beanFactoryPostProcessor.postProcessBeanFactory(beanFactory);
        }

        //2、再找在ioc容器创建的 BeanFactoryPostProcessor
        Map<String, BeanFactoryPostProcessor> beanFactoryPostProcessorMap = beanFactory.getBeansOfType(BeanFactoryPostProcessor.class);
        for (BeanFactoryPostProcessor beanFactoryPostProcessor : beanFactoryPostProcessorMap.values()) {
            beanFactoryPostProcessor.postProcessBeanFactory(beanFactory);
        }
    }

    @Override
    public Object getBean(String beanName) throws BeansException {
        return getBeanFactory().getBean(beanName);
    }

    @Override
    public Object getBean(String beanName, Object... args) throws BeansException {
        return getBeanFactory().getBean(beanName, args);
    }

    @Override
    public <T> T getBean(String name, Class<T> requiredType) throws BeansException {
        return getBeanFactory().getBean(name, requiredType);
    }

    @Override
    public <T> T getBean(Class<T> requiredType) throws BeansException {
        return getBeanFactory().getBean(requiredType);
    }

    @Override
    public boolean containsBeanDefinition(String beanName) {
        return getBeanFactory().containsBeanDefinition(beanName);
    }

    @Override
    public <T> Map<String, T> getBeansOfType(Class<T> type) throws BeansException {
        return getBeanFactory().getBeansOfType(type);
    }

    @Override
    public String[] getBeanDefinitionNames() {
        return getBeanFactory().getBeanDefinitionNames();
    }

    public List<BeanFactoryPostProcessor> getBeanFactoryPostProcessors() {
        return beanFactoryPostProcessors;
    }

    @Override
    public void addBeanFactoryPostProcessor(BeanFactoryPostProcessor postProcessor) {
        Assert.notNull(postProcessor, "BeanFactoryPostProcessor must not be null");
        this.beanFactoryPostProcessors.add(postProcessor);
    }

    @Override
    public BeanFactory getParentBeanFactory() {
        return null;
    }

    @Override
    public void registerShutdownHook() {
        if(this.shutdownHook == null){
            this.shutdownHook = new Thread(this::close);
            Runtime.getRuntime().addShutdownHook(shutdownHook);
        }
    }

    @Override
    public void close() {

        doClose();
        // 移除关闭钩子， 这里不加锁会报错
//        if(this.shutdownHook != null){
//            Runtime.getRuntime().removeShutdownHook(shutdownHook);
//        }
    }

    /**
     * 关闭资源
     */
    protected void doClose(){
        //发布销毁事件
        publishEvent(new ContextClosedEvent(this));

        //销毁单例容器
        getBeanFactory().destroySingletons();
    }
}
