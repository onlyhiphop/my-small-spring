package org.bcliao.example2;

import org.bcliao.bean.UserService;
import org.junit.Test;

/**
 * @author bcliao
 * @date 2021/12/20 10:46
 */
public class BeanFactory2Test {

    @Test
    public void test_BeanDefinition(){
        // 1.初始化 BeanFactory
        BeanFactory2 beanFactory = new BeanFactory2();

        // 2.注入bean
        BeanDefinition beanDefinition = new BeanDefinition(new UserService());
        beanFactory.registerBeanDefinition("userService", beanDefinition);

        // 3.获取bean
        UserService userService = (UserService) beanFactory.getBean("userService");
        userService.findUserInfo();
    }
}
