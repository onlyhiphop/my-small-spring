package org.bcliao.beans.factory;

import org.bcliao.beans.BeansException;

/**
 * @author bcliao
 * @date 2022/2/28 10:47
 */
public interface ObjectFactory<T> {

    T getObject() throws BeansException;

}
