#### IOC

---

​	在上一章节中，`AbstractAutowireCapableBeanFactory` 中 的 createBean 方法还存在缺陷，它只能调用对象的无参构造方法。

​	所以我们在 `BeanFactory` 接口中增加一个带参数的方法

```java
Object getBean(String beanName, Object[] args);
```

​	这样，在调用 createBean  方法时就可以传入 getBean 带来的参数去构建。这部分比较简单，不做过多解释，详情看代码。

​	得到参数后，我们就可以去调用有参的构造方法，这里我们还可以做扩展，我们知道构建对象有两种方式，JDK反射和cglib，看完了上一章后，这时候你是否也想到了把不同的实例化策略功能抽出一个接口？

​	设计 `InstantiationStrategy` 创建实例策略接口，然后分别创建两个策略实现类  `SimpleInstantiationStrategy` 和 `CglibSubclassingInstantiationStrategy`

​	最后 `AbstractAutowireCapableBeanFactory` 组合`InstantiationStrategy` ，开放实例化策略get、set方法，后续可以通过set方法设置具体实例化策略。



#### 总结

​	在外面需要新增一个带参数的方法时，发现我们实际使用的 `DefaultListableBeanFactory` 不需要改动，我们创建类的动作已经在上层模板类中 `AbstractAutowireCapableBeanFactory` 已经处理了，我们在代码设计时，要尽可能的去拆分好每个类的职责，这样才不会因为需求的增加导致类结构混乱和大面积改动。

​	面对实例化操作，我们使用了策略模式，可以针对不同场景使用不同的实例化策略。

