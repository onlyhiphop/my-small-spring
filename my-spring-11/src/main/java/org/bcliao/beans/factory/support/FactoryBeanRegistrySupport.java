package org.bcliao.beans.factory.support;

import org.bcliao.beans.BeansException;
import org.bcliao.beans.factory.FactoryBean;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author bcliao
 * @date 2022/1/16 19:30
 */
public abstract class FactoryBeanRegistrySupport extends DefaultSingletonBeanRegistry{

    /**
     * 由FactoryBeans创建的单例对象的缓存
     */
    private final Map<String, Object> factoryBeanObjectCache = new ConcurrentHashMap<>(16);

    protected Object getCachedObjectForFactoryBean(String beanName){
        return this.factoryBeanObjectCache.get(beanName);
    }

    protected Object getObjectFromFactoryBean(FactoryBean factoryBean, String beanName){
        if(factoryBean.isSingleton()){
            Object object = this.factoryBeanObjectCache.get(beanName);
            if(object == null){
                object = doGetObjectFromFactoryBean(factoryBean, beanName);

                //调用后置处理器
                object = postProcessObjectFromFactoryBean(object, beanName);

                //放入单例容器中
                this.factoryBeanObjectCache.put(beanName, object);
            }
            return object;
        }else{
            //非单例模式
            return doGetObjectFromFactoryBean(factoryBean, beanName);
        }
    }

    private Object doGetObjectFromFactoryBean(FactoryBean factoryBean, String beanName){
        try {
            return factoryBean.getObject();
        }catch (Exception e){
            throw new BeansException("FactoryBean threw exception on object[" + beanName + "] creation", e);
        }
    }

    /**
     * 执行 BeanPostProcessor 的 postProcessAfterInitialization 后置处理方法
     */
    protected Object postProcessObjectFromFactoryBean(Object object, String beanName) throws BeansException{
        return object;
    }
}
