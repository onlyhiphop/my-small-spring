package org.bcliao.beans;

/**
 * @author bcliao
 * @date 2021/12/24 10:27
 */
public class PropertyValue {

    private final String beanName;
    private final Object value;

    public PropertyValue(String beanName, Object value) {
        this.beanName = beanName;
        this.value = value;
    }

    public String getBeanName() {
        return beanName;
    }
    public Object getValue() {
        return value;
    }

}
