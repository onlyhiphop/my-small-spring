package org.bcliao.test.bean;

import org.bcliao.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * @author bcliao
 * @date 2022/2/22 9:13
 */
public class UserServiceBeforeAdvice implements MethodBeforeAdvice {

    public UserServiceBeforeAdvice(){
        System.out.println("UserServiceBeforeAdvice---Construct---");
    }

    @Override
    public void before(Method method, Object[] args, Object target) throws Throwable {
        System.out.println("拦截方法-------- " + method.getName());
    }
}
