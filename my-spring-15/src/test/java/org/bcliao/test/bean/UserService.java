package org.bcliao.test.bean;

import org.bcliao.beans.factory.annotation.Autowired;
import org.bcliao.beans.factory.annotation.Value;
import org.bcliao.stereotype.Component;

/**
 * @author bcliao
 * @date 2022/2/11 10:47
 */
@Component("userService")
public class UserService implements IUserService{

    @Value("${token}")
    private String token;

    @Autowired
    private UserDao userDao;

    @Override
    public String queryUserInfo() {
        return userDao.queryUserName(token);
    }
}
