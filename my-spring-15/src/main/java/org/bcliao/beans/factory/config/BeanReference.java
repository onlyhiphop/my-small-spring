package org.bcliao.beans.factory.config;

/**
 * @author bcliao
 * @date 2021/12/24 11:19
 */
public class BeanReference {
    private final String beanName;

    public BeanReference(String beanName) {
        this.beanName = beanName;
    }

    public String getBeanName() {
        return beanName;
    }
}
