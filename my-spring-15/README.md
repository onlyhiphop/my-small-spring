#### AOP

---



##### 目标

​	实现 `@Autowired` 、 `@Value` 、`@Qualifier` 自动注入注解



##### 结构图

![image-20220207095755185](img/image-20220207095755185.png)



##### 设计

- `InstantiationAwareBeanPostProcessor` 新增方法 `postProcessPropertyValues` 
-  新增 `AutowiredAnnotationBeanPostProcessor` 类去实现 `InstantiationAwareBeanPostProcessor` 接口中的 `postProcessPropertyValues` 方法，该方法中去解析 `@Value`、`@Autowired` 、`@Qualifier` 注解设置 Bean 的属性
-  给工厂加上 字符串解析功能 ，在 `ConfigurableBeanFactory` 接口添加 `addEmbeddedValueResolver` 、`resolveEmbeddedValue` 方法，最后在 `AbstractBeanFactory` 中实现，`AutowiredAnnotationBeanPostProcessor` 中就可以拿到 BeanFactory 去解析 @Value 的值
-  `AutowiredAnnotationBeanPostProcessor` 解析 `@Value` 逻辑中，将字符串的解析功能抽象成 `StringValueResolver` 接口，`AbstractBeanFactory#embeddedValueResolvers` 存储了所有字符串解析器，解析占位符字符串逻辑主要在 `AbstractBeanFactory#resolveEmbeddedValue`中可以发现，它是遍历所有的字符解析器得到结果（这里设计成了可扩展，我们可以叠加自己的字符解析器）。
- `PropertyPlaceholderConfigurer#postProcessBeanFactory` 中注册一个字符解析器
- 在 `ClassPathBeanDefinitionScanner#doScan` 中将 `AutowiredAnnotationBeanPostProcessor` 注册到容器中
- 在 `AbstractAutowireCapableBeanFactory#createBean` 中 实例化Bean 之后，注入属性之前，加入 执行`InstantiationAwareBeanPostProcessor#postProcessPropertyValues` 接口修改属性值



##### 测试

org.bcliao.test.MainTest



##### 总结

​	我们一直是围绕在 Bean 的生命周期中进行处理，不停的使用 BeanPostProcessor 去扩展我们的新功能，有时候需要做一些差异化的控制，所以需要继承 BeanPostProcessor 接口，定义新接口 InstantiationAwareBeanPostProcessor 这样就可以区分出不同扩展点的操作了。慢慢就发现在之前 Bean 生命周期中各个部分细分非常好，可以让你在任何初始化的时间点上，任何面上，都能做你想做的扩展和改变，具有极好的灵活性。
