package org.bcliao.context;

import java.util.EventListener;

/**
 * @author bcliao
 * @date 2022/1/20 16:33
 */
public interface ApplicationListener<E extends ApplicationEvent> extends EventListener {

    /**
     * 处理一个 ApplicationEvent
     */
    void onApplicationEvent(E event);
}
