package org.bcliao.test.bean;

/**
 * @author bcliao
 * @date 2022/1/16 17:02
 */
public class UserService {

    private IUserDao userDao;

    public String queryUserInfo(String uId){
        return userDao.queryUserName(uId);
    }

    public void destroyMethod(){
        System.out.println("UserService 执行了销毁方法--------");
    }
}
