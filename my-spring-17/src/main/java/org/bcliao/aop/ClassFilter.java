package org.bcliao.aop;

/**
 * @author bcliao
 * @date 2022/1/22 20:01
 */
public interface ClassFilter {

    /**
     * 对类进行过滤，是否符合切入点表达式
     */
    boolean matches(Class<?> clazz);

}
