package org.bcliao.aop;

import org.aopalliance.aop.Advice;

/**
 * spring 提供的顾问，用来获取使用适用 advice
 * @author bcliao
 * @date 2022/1/26 15:51
 */
public interface Advisor {

    /**
     * Return the advice part of this aspect. An advice may be an
     * interceptor, a before advice, a throws advice, etc.
     * @return the advice that should apply if the pointcut matches
     */
    Advice getAdvice();
}
