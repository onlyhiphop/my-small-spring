#### IOC

spring容器管理着对象的配置和生命周期

1、选择合适的容器。

常用的容器有ArrayList、LinkedList、HashSet、HashMap等，由于我们要经常从容器中精确的拿到某个对象，很显然选择HashMap是最合适不过的。



2、初步设计

详情见包中代码： `org.bcliao.example1`

建立一个BeanFactory工厂类来创建、获取对象

将Bean的名称作为Key，实例对象作为值

```
map.put("userService", new UserService());
```



3、优化设计

详情见包中代码： `org.bcliao.example2`

将容器和实例对象解耦，创建 BeanDefinition 对象

这样容器获取的不只是一个实例对象，还可以包括其他类信息
