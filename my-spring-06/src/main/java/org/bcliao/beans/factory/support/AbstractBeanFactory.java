package org.bcliao.beans.factory.support;

import org.bcliao.beans.BeansException;
import org.bcliao.beans.factory.BeanFactory;
import org.bcliao.beans.factory.config.BeanDefinition;
import org.bcliao.beans.factory.config.ConfigurableBeanFactory;

/**
 * @author bcliao
 * @date 2021/12/21 10:55
 */
public abstract class AbstractBeanFactory extends DefaultSingletonBeanRegistry implements ConfigurableBeanFactory {

    /**
     * 父bean工厂，用于bean继承支持
     */
    private BeanFactory parentBeanFactory;

    @Override
    public Object getBean(String beanName) throws BeansException {
        return doGetBean(beanName, null);
    }

    @Override
    public Object getBean(String beanName, Object... args) throws BeansException {
        return doGetBean(beanName, args);
    }

    protected <T> T doGetBean(String beanName, Object[] args){
        //尝试从单例容器缓存中获取
        Object singleton = getSingleton(beanName);
        if(singleton != null){
            return (T) singleton;
        }
        //如果单例容器中不存在，就拿到 BeanDefinition 自己创建
        BeanDefinition beanDefinition = getBeanDefinition(beanName);
        return (T) createBean(beanName, beanDefinition, args);
    }

    protected abstract BeanDefinition getBeanDefinition(String beanName) throws BeansException;

    protected abstract Object createBean(String beanName, BeanDefinition beanDefinition, Object[] args) throws BeansException;

    @Override
    public BeanFactory getParentBeanFactory() {
        return this.parentBeanFactory;
    }
}