package org.bcliao.core.convert.converter;

/**
 * 类型转换处理接口
 * @author bcliao
 * @date 2022/3/8 9:39
 */
public interface Converter<S, T> {

    T convert(S source);
}
