package org.bcliao.core.io;

public interface ResourceLoader {

    /**
     * 设置 classpath 的常量
     */
    String CLASSPATH_URL_PREFIX = "classpath:";

    Resource getResource(String location);

}
