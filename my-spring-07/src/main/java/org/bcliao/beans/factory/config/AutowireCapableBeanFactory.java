package org.bcliao.beans.factory.config;

import org.bcliao.beans.BeansException;
import org.bcliao.beans.factory.BeanFactory;

/**
 * @author bcliao
 * @date 2021/12/28 14:09
 */
public interface AutowireCapableBeanFactory extends BeanFactory {
    /**
     * 表明没有自动注入
     */
    int AUTOWIRE_NO = 0;

    /**
     * 按名称关联自动注入
     */
    int AUTOWIRE_BY_NAME = 1;

    /**
     * 按类型关联自动注入
     */
    int AUTOWIRE_BY_TYPE = 2;

    /**
     * 按构造器关联自动注入
     */
    int AUTOWIRE_CONSTRUCTOR = 3;

//    void autowireBean(Object existingBean) throws BeansException;

    /**
     * 调用 BeanPostProcessor 的 postprocessebeforeignization 应用于给定的实例
     * @throws BeansException
     */
    Object applyBeanPostProcessorsBeforeInitialization(Object existingBean, String beanName) throws BeansException;

    /**
     * 调用 BeanPostProcessor 的 postProcessAfterInitialization 应用于给定给定的实例
     */
    Object applyBeanPostProcessorsAfterInitialization(Object existingBean, String beanName) throws BeansException;

}
