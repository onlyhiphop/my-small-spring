package org.bcliao.factory.support;

import org.bcliao.factory.BeansException;
import org.bcliao.factory.config.BeanDefinition;

import java.lang.reflect.Constructor;

/**
 * @author bcliao
 * @date 2021/12/23 9:53
 */
public interface InstantiationStrategy {
    Object instantiate(BeanDefinition beanDefinition, String beanName, Constructor constructor, Object[] args) throws BeansException;
}
