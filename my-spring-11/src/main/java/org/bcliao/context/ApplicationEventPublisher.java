package org.bcliao.context;

/**
 * @author bcliao
 * @date 2022/1/20 16:35
 */
public interface ApplicationEventPublisher {

    /**
     * 通知在此应用程序注册的所有监听器
     */
    void publishEvent(ApplicationEvent event);
}
