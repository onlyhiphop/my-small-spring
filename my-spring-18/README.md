#### 数据类型转换
---



##### 目标

​	在属性注入时，要将值转换成对应实体类属性类型（常在SpringMvc用来处理参数绑定），为了复用和后续扩展，将实现一个通用的数据类型转换模块。



##### 设计

首先写出扩展性强的代码，肯定面向接口编程，和前面章节一样，我们需要将功能细化，每个功能再接口化。

1、设计顶级接口

- `Converter<S, T>`  ：类型转换处理接口
- `ConverterFactory<S, R>` ：类型转换工厂，从中得到各种类型转换器
- `ConverterRegistry` ： 类型转换器注册接口，用于注册转换器
- `ConversionService` ：类型转换服务接口，提供类型转换服务，用来记录各种类型转换器
- `GenericConverter`： 再封装一个通用的转换器，用于包装转换器

![image-20220311132709945](img/image-20220311132709945.png)

由此图可知，`ConversionService` 就是将其他功能接口结合起来，提供给外部使用的。



2、将类型转换功能接入Spring中

-  `ConversionServiceFactoryBean` 使用 `FactoryBean` 接口 将通用的 `GenericConversionService` 转换服务注册到 Spring 容器中，属性 `converters` 提供给外界注入 自定义的转换器，在初始化 `afterPropertiesSet` 时就会将外界注入的 `converters`  注册到服务类中  `GenericConversionService#converters` 



##### 结构图

![image-20220311155049378](img/image-20220311155049378.png)



##### 测试

org.bcliao.test



##### 总结

我们可以从类型转换功能设计上学习到如何去设计一个具有通用性扩展性极强的模块，再通过使用我们之前学习到的 Spring 中的扩展接口将其融入 Spring 中。其中类型转换的设计融进了 IOC 中，这样就可以利用了 SPI 的思想，将转换器的实现 交给用户，用户只需要实现对应的接口，就可以自定义想要的转换器。