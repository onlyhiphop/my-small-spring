package org.bcliao.test.event;

import org.bcliao.context.ApplicationContext;
import org.bcliao.context.ApplicationEvent;

/**
 * @author bcliao
 * @date 2022/1/21 11:23
 */
public class CustomEvent extends ApplicationEvent {

    private String message;

    private ApplicationContext context;

    public CustomEvent(Object source, String message) {
        super(source);
        context = (ApplicationContext) source;
        this.message = message;
    }

    public String getMessage() {
        return message + " 获取userName: " + ((User)context.getBean("user")).getUserName();
    }

}
