package org.bcliao.context;

import java.util.EventObject;

/**
 * @author bcliao
 * @date 2022/1/20 15:37
 */
public class ApplicationEvent extends EventObject {

    /**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public ApplicationEvent(Object source) {
        super(source);
    }
}
