package org.bcliao.beans.factory.support;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.NoOp;
import org.bcliao.beans.BeansException;
import org.bcliao.beans.factory.config.BeanDefinition;

import java.lang.reflect.Constructor;

/**
 * @author bcliao
 * @date 2021/12/23 11:16
 */
public class CglibSubclassingInstantiationStrategy implements InstantiationStrategy{
    @Override
    public Object instantiate(BeanDefinition beanDefinition, String beanName, Constructor constructor, Object[] args) throws BeansException {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(beanDefinition.getBeanClass());
        enhancer.setCallback(new NoOp() {
            @Override
            public int hashCode() {
                return super.hashCode();
            }
        });
        if(constructor == null) return enhancer.create();
        return enhancer.create(constructor.getParameterTypes(), args);
    }
}
