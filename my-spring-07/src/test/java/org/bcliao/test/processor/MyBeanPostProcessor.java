package org.bcliao.test.processor;

import org.bcliao.beans.BeansException;
import org.bcliao.beans.factory.config.BeanPostProcessor;
import org.bcliao.test.bean.UserService;

/**
 * @author bcliao
 * @date 2022/1/10 11:30
 */
public class MyBeanPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("BeanPostProcessor 在初始化之前------::" + beanName);
        if ("userService".equals(beanName)) {
            UserService userService = (UserService) bean;
            userService.setLocation("改为：北京");
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("BeanPostProcessor 在初始化之后------::" + beanName);
        return bean;
    }

}
