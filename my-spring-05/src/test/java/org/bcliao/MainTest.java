package org.bcliao;

import cn.hutool.core.io.IoUtil;
import org.bcliao.bean.UserService;
import org.bcliao.beans.factory.support.DefaultListableBeanFactory;
import org.bcliao.beans.factory.xml.XmlBeanDefinitionReader;
import org.bcliao.core.io.DefaultResourceLoader;
import org.bcliao.core.io.Resource;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author bcliao
 * @date 2021/12/27 13:29
 */
public class MainTest {

    private DefaultResourceLoader resourceLoader;

    @Before
    public void before(){
        resourceLoader = new DefaultResourceLoader();
    }

    @Test
    public void test_classpath() throws IOException {
        Resource resource = resourceLoader.getResource("classpath:test.properties");
        InputStream inputStream = resource.getInputStream();
        String content = IoUtil.readUtf8(inputStream);
        System.out.println(content);
    }

    @Test
    public void test_file() throws IOException {
        Resource resource = resourceLoader.getResource("src/test/resources/test.properties");
        InputStream inputStream = resource.getInputStream();
        String content = IoUtil.readUtf8(inputStream);
        System.out.println(content);
    }

    @Test
    public void test_url() throws IOException {
        Resource resource = resourceLoader.getResource("https://gitee.com/onlyhiphop/my-small-spring/blob/master/pom.xml");
        InputStream inputStream = resource.getInputStream();
        String content = IoUtil.readUtf8(inputStream);
        System.out.println(content);
    }

    @Test
    public void test_XmlBeanDefinitionReader(){
        // 初始化工厂
        DefaultListableBeanFactory factory = new DefaultListableBeanFactory();

        // 读取xml来注册bean
        XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(factory, new DefaultResourceLoader());
        reader.loadBeanDefinitions("classpath:spring.xml");

        // 获取对象
        UserService userService = (UserService) factory.getBean("userService");
        String result = userService.queryUserInfo();
        System.out.println("结果：" + result);
    }
}
