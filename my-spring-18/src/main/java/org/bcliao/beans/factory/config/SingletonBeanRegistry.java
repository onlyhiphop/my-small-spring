package org.bcliao.beans.factory.config;

/**
 * @author bcliao
 * @date 2021/12/21 10:41
 */
public interface SingletonBeanRegistry {
    Object getSingleton(String beanName);
    void registerSingleton(String beanName, Object singletonObject);
}
