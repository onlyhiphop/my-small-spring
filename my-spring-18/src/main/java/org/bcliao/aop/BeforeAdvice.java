package org.bcliao.aop;

import org.aopalliance.aop.Advice;

/**
 * @author bcliao
 * @date 2022/1/26 15:42
 */
public interface BeforeAdvice extends Advice {
}
