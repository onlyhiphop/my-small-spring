package org.bcliao.aop;

/**
 * @author bcliao
 * @date 2022/1/22 20:04
 */
public interface Pointcut {

    ClassFilter getClassFilter();

    MethodMatcher getMethodMatcher();

}
