package org.bcliao.example2;

/**
 * @author bcliao
 * @date 2021/12/20 10:45
 */
public class BeanDefinition {

    private Object bean;

    public BeanDefinition(Object bean){
        this.bean = bean;
    }

    public Object getBean() {
        return bean;
    }
}
