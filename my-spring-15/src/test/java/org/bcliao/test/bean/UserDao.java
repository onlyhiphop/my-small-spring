package org.bcliao.test.bean;

import org.bcliao.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author bcliao
 * @date 2022/2/11 10:46
 */
@Component
public class UserDao {

    private static Map<String, String> maps = new HashMap<>();

    static {
        maps.put("001", "A");
        maps.put("002", "B");
        maps.put("003", "C");
    }

    public String queryUserName(String uId){
        return maps.get(uId);
    }
}
