package org.bcliao.context;

import org.bcliao.beans.BeansException;
import org.bcliao.beans.factory.config.BeanFactoryPostProcessor;

/**
 * @author bcliao
 * @date 2022/1/4 10:29
 */
public interface ConfigurableApplication extends ApplicationContext {

    /**
     * 加载或刷新容器配置
     */
    void refresh() throws BeansException;

    /**
     * 手动添加 BeanFactoryPostProcessor
     */
    void addBeanFactoryPostProcessor(BeanFactoryPostProcessor postProcessor);

    /**
     * 注册一个JVM关闭钩子
     */
    void registerShutdownHook();

    /**
     * 手动关闭资源
     */
    void close();
}
