package org.bcliao.test;


import org.bcliao.context.support.ClassPathXmlApplicationContext;
import org.bcliao.test.bean.User;
import org.junit.Test;

/**
 * @author bcliao
 * @date 2022/3/8 13:59
 */
public class MainTest {

    @Test
    public void test_converter(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring-scan.xml");
        User user = (User) context.getBean("user");
        System.out.println(user);
    }
}
