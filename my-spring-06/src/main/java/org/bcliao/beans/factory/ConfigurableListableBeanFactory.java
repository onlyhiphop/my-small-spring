package org.bcliao.beans.factory;

import org.bcliao.beans.BeansException;
import org.bcliao.beans.factory.config.AutowireCapableBeanFactory;
import org.bcliao.beans.factory.config.BeanDefinition;
import org.bcliao.beans.factory.config.ConfigurableBeanFactory;

/**
 * @author bcliao
 * @date 2021/12/28 14:16
 */
public interface ConfigurableListableBeanFactory extends ListableBeanFactory, AutowireCapableBeanFactory, ConfigurableBeanFactory {

    BeanDefinition getBeanDefinition(String beanName) throws BeansException;
}
