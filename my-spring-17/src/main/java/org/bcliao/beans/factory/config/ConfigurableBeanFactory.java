package org.bcliao.beans.factory.config;

import org.bcliao.beans.factory.HierarchicalBeanFactory;
import org.bcliao.util.StringValueResolver;

/**
 * @author bcliao
 */
public interface ConfigurableBeanFactory extends HierarchicalBeanFactory, SingletonBeanRegistry {

    String SCOPE_SINGLETON = "singleton";
    String SCOPE_PROTOTYPE = "prototype";

    void addBeanPostProcessor(BeanPostProcessor beanPostProcessor);

    /**
     * 销毁单例对象
     */
    void destroySingletons();

    /**
     * 为注释属性等嵌入值添加字符串解析器
     * @param valueResolver 要应用于嵌入值的字符串解析器
     */
    void addEmbeddedValueResolver(StringValueResolver valueResolver);

    /**
     * 解析给定的嵌入值，例如注释属性
     * @param value 要解析的值
     * @return 解析值（可以是原始值）
     */
    String resolveEmbeddedValue(String value);
}
