package org.bcliao.test.bean;

import org.bcliao.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * @author bcliao
 * @date 2022/2/14 13:12
 */
public class UserServiceBeforeAdvice implements MethodBeforeAdvice {

    @Override
    public void before(Method method, Object[] args, Object target) throws Throwable {
        System.out.println("拦截方法：" + method.getName());
    }

}
