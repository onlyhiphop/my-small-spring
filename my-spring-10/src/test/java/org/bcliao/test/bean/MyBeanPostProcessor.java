package org.bcliao.test.bean;

import org.bcliao.beans.BeansException;
import org.bcliao.beans.factory.config.BeanPostProcessor;

/**
 * @author bcliao
 * @date 2022/1/20 8:54
 */
public class MyBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if(bean instanceof IUserDao){
            System.out.println("IUserDao 的实现类，执行 BeanPostProcessor Before------");
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if(bean instanceof IUserDao){
            System.out.println("IUserDao 的实现类，执行 BeanPostProcessor After------");
        }
        return bean;
    }
}
