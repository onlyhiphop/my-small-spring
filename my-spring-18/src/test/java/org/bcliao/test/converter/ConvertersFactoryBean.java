package org.bcliao.test.converter;

import org.bcliao.beans.factory.FactoryBean;

import java.util.HashSet;
import java.util.Set;

/**
 * 创建多个Converter
 * @author bcliao
 * @date 2022/3/11 14:11
 */
public class ConvertersFactoryBean implements FactoryBean<Set<?>> {

    @Override
    public Set<?> getObject() throws Exception {
        Set<Object> converters = new HashSet<>();
        converters.add(new StringToLocalDateConverter("yyyy/MM/dd"));
        converters.add(new StringToDoubleConverter());
        return converters;
    }

    @Override
    public Class<?> getObjectType() {
        return Set.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
