package org.bcliao.aop.framework;

/**
 * 代理的抽象接口
 * @author bcliao
 * @date 2022/1/23 10:21
 */
public interface AopProxy {

    Object getProxy();

}
