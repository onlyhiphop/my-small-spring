package org.bcliao.test;

import org.bcliao.context.support.ClassPathXmlApplicationContext;
import org.bcliao.test.event.CustomEvent;
import org.junit.Test;

/**
 * @author bcliao
 * @date 2022/1/21 9:29
 */
public class MainTest {

    @Test
    public void test_HadEvent(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring.xml");
        context.registerShutdownHook();
    }

    @Test
    public void test_CustomEvent(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring.xml");
        context.publishEvent(new CustomEvent(context, "自定义事件----"));
    }

}
