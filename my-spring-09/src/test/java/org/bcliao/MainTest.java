package org.bcliao;

import org.bcliao.bean.BeanAware;
import org.bcliao.context.support.ClassPathXmlApplicationContext;
import org.junit.Test;

/**
 * @author bcliao
 * @date 2022/1/14 15:56
 */
public class MainTest {

    @Test
    public void test_aware(){
        ClassPathXmlApplicationContext applicationContextContext = new ClassPathXmlApplicationContext("classpath:spring.xml");
        BeanAware beanAware = (BeanAware) applicationContextContext.getBean("beanAware");
        System.out.println(beanAware.toString());
    }
}
