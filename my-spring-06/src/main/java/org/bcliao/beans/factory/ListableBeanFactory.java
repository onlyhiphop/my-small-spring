package org.bcliao.beans.factory;

import org.bcliao.beans.BeansException;

import java.util.Map;

/**
 * @author bcliao
 */
public interface ListableBeanFactory extends BeanFactory {


    /**
     * 是否包含 BeanDefinition
     */
    boolean containsBeanDefinition(String beanName);

    /**
     * 按照类型返回 Bean 实例
     */
    <T> Map<String, T> getBeanOfType(Class<T> type) throws BeansException;

    /**
     * 返回注册表中所有的 Bean 的名称
     */
    String[] getBeanDefinitionNames();

}
